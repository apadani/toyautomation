/*
 * 
 */
package com.test.espanol.mobile;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Guice;
import org.testng.annotations.Test;

import com.demo.constant.GlobalPagesConstant;
import com.demo.datamodel.RequestQuoteModel;
import com.demo.dataproviders.DataProvidersEsp;
import com.demo.gmail.GmailAPI;
import com.demo.pages.HomePage;
import com.demo.pages.RequestAQuotePage;
import com.demo.selenium.core.BaseTest;
import com.demo.utilities.Utilities;
import com.demo.validation.ValidationData;

@Guice
public class RAQStandaloneEspMobile extends BaseTest {
	
	public RAQStandaloneEspMobile(){
		mobileTest = true;
	}
	
	@Test(dataProvider = "Standalone", dataProviderClass = DataProvidersEsp.class)
	public void RAQStandaloneEspMobileTest(RequestQuoteModel requestQuoteModel) throws Exception {
		
		HomePage homePage = PageFactory.initElements(getWebDriver(), HomePage.class);
		RequestAQuotePage requestAQuotePage = PageFactory.initElements(getWebDriver(), RequestAQuotePage.class);
		requestQuoteModel.setCommentText(super.getComment("Test ESP Mobile RAQ Standalone"));
		requestQuoteModel.setCampaignCode("tmtt00340000");
		requestQuoteModel.setSiteName("mobile");
		
		requestAQuotePage.submitZipCodeMobile(requestQuoteModel);
		reportLog("Submit zip from popup");
		
		homePage.selectEspanolLanguageMobile();
		reportLog("select espanol language");
		
		homePage.gotoRequestQuoteMobileEsp();
		reportLog("click on request a quote menu from shopping tool menu");
		
		requestAQuotePage.fillRequestQuoteDetail(requestQuoteModel);
		requestQuoteModel.setVendorName(requestAQuotePage.getVendorName());

		requestQuoteModel.setDealerCode(requestAQuotePage.dealerCode());
		reportLog("fill request detail " + requestQuoteModel.toString());

		//GmailAPI.delete(requestQuoteModel.getEmail(), gmailPass);
		requestAQuotePage.clickSubmitButton();
		reportLog("click on submit button");

		String message = requestAQuotePage.getMessage();
		Assert.assertEquals(message.trim(), GlobalPagesConstant.RequestSentEsp);
		reportLog("Verify message request send successfully");

		String raqConfirmationMessage = requestAQuotePage.getConfirmationThankYouMessage();
		String raqConfirmationGrade = requestAQuotePage.getConfirmationGradeMessage();
		System.out.println(raqConfirmationGrade);

		Assert.assertTrue(raqConfirmationMessage.trim().toLowerCase().contains(
				GlobalPagesConstant.ThankYouConfirmationEsp.toLowerCase() + " "
						+ requestQuoteModel.getSeriesName().toLowerCase()));
		reportLog("Verify message Thank you! A dealer will contact you soon with a quote on your new Vehicle.");

		//Assert.assertEquals(raqConfirmationGrade.trim(),
				//requestQuoteModel.getSeriesName().trim() + " " + requestQuoteModel.getModelName().trim());
		reportLog("Verify RAQ confirmation grade message with Vehicle info");

		requestAQuotePage.verifyButtonsEsp();
		reportLog("Verify 'Search vender' and 'Build your own' buttons");

		String from = "tmsusaincsaleslead@tmsusaconnect.com";
		String mailContent = GmailAPI.check(requestQuoteModel.getEmail(), gmailPass, from,
				requestQuoteModel.getCommentText());
		reportLog("Read email content");
		reportLog(mailContent);
		//GmailAPI.delete(requestQuoteModel.getEmail(), gmailPass);
		//reportLog("Delete emails");

		Assert.assertNotEquals("Not found", mailContent, "Email not recieved in 2 minutes");
		ValidationData.validateRequestQuoteTextEmailData(requestQuoteModel, mailContent, "Dafault");
		reportLog("Verify email content successfully"); 
	}

}
