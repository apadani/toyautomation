package com.test.espanol.mobile;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Guice;
import org.testng.annotations.Test;

import com.demo.constant.GlobalPagesConstant;
import com.demo.datamodel.RequestQuoteModel;
import com.demo.dataproviders.DataProvidersEsp;
import com.demo.gmail.GmailAPI;
import com.demo.pages.HomePage;
import com.demo.pages.RequestAQuotePage;
import com.demo.pages.SearchInventoryPage;
import com.demo.selenium.core.BaseTest;
import com.demo.utilities.Utilities;
import com.demo.validation.ValidationData;

@Guice
public class RAQSearchInventoryEspMobile extends BaseTest {

	public RAQSearchInventoryEspMobile(){
		mobileTest = true;
	}
	
	@Test(dataProvider = "SearchInventory", dataProviderClass=DataProvidersEsp.class)
	public void testRAQSearchInventoryEspMobile(RequestQuoteModel requestQuoteModel) throws Exception{
		
		RequestAQuotePage requestAQuotePage = PageFactory.initElements(getWebDriver(),
				RequestAQuotePage.class);		
		HomePage homePage = PageFactory.initElements(getWebDriver(), HomePage.class);
		
		requestQuoteModel.setCommentText(super.getComment("Test ESP Mobile RAQ Search And Inventory"));
		requestQuoteModel.setCampaignCode("tmtt31840000");
		requestQuoteModel.setSiteName("mobile");
		
		homePage.selectEspanolLanguageMobile();
		reportLog("select espanol language");
		
		requestAQuotePage.submitZipCodeMobile(requestQuoteModel);
		reportLog("submit zip code");
		
		SearchInventoryPage searchInventoryPage = homePage.gotoSearchInventoryEspMobilePage();
		reportLog("Go to search inventory page from shoppig menu");
		
		searchInventoryPage.selectCarBySeriesEsp(requestQuoteModel);
		reportLog("Select car from search inventory page");
		
		searchInventoryPage.clickViewDetailsEsp();
		reportLog("click on view details page");
		
		requestAQuotePage.fillRQDetail(requestQuoteModel);
		requestQuoteModel.setVendorName(requestAQuotePage.getVendorName());
		requestQuoteModel.setDealerCode(requestAQuotePage.dealerCode());
		requestQuoteModel = searchInventoryPage.getPriceEspMobile(requestQuoteModel);
		String currentUrl = getWebDriver().getCurrentUrl();
		requestQuoteModel = Utilities.setColor(requestQuoteModel, currentUrl);
		reportLog("fill request detail " + requestQuoteModel.toString());
		
		//GmailAPI.delete(requestQuoteModel.getEmail(), gmailPass);
		requestAQuotePage.clickSubmitButton();
		reportLog("click on submit button");
				
		String message = requestAQuotePage.getMessage();
		Assert.assertEquals(message.trim(), GlobalPagesConstant.RequestSentEsp);
		reportLog("Verify message request send successfully");
			
		String raqConfirmationMessage = requestAQuotePage.getConfirmationThankYouMessage();
		String year = Utilities.getSeriesYearEsp(requestQuoteModel);
		String seriesName = Utilities.getSeriesNameEsp(requestQuoteModel);
		Assert.assertTrue(raqConfirmationMessage.trim().toLowerCase().contains(
				GlobalPagesConstant.ThankYouConfirmationEsp.toLowerCase() + " "
						+ year.toLowerCase() + " "+ seriesName.toLowerCase()));		
		reportLog("Verify message Thank you! A dealer will contact you soon with a quote on your new Vehicle.");

		String from = "tmsusaincsaleslead@tmsusaconnect.com";
		String mailContent = GmailAPI.check(requestQuoteModel.getEmail(), gmailPass, from,
				requestQuoteModel.getCommentText());
		reportLog("Read email content");
		reportLog(mailContent);
		//GmailAPI.delete(requestQuoteModel.getEmail(), gmailPass);
		//reportLog("Delete emails");
		
		Assert.assertNotEquals("Not found", mailContent, "Email not recieved in 2 minutes");
		ValidationData.validateRequestQuoteTextEmailData(requestQuoteModel, mailContent, "Esp");
		reportLog("Verify email content successfully"); 
	}
}