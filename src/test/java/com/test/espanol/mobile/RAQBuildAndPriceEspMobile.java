package com.test.espanol.mobile;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Guice;
import org.testng.annotations.Test;

import com.demo.constant.GlobalPagesConstant;
import com.demo.datamodel.RequestQuoteModel;
import com.demo.dataproviders.DataProvidersEsp;
import com.demo.gmail.GmailAPI;
import com.demo.pages.BuildAndPricePage;
import com.demo.pages.HomePage;
import com.demo.pages.RequestAQuotePage;
import com.demo.selenium.core.BaseTest;
import com.demo.utilities.Utilities;
import com.demo.validation.ValidationData;

@Guice
public class RAQBuildAndPriceEspMobile extends BaseTest{
	
	public RAQBuildAndPriceEspMobile(){
		mobileTest = true;
	}
	
	@Test(dataProvider = "BuildAndPrice", dataProviderClass=DataProvidersEsp.class)
	public void RAQBuildAndPriceEspMobileTest(RequestQuoteModel requestQuoteModel) throws Exception{
		
		RequestAQuotePage requestAQuotePage = PageFactory.initElements(getWebDriver(),
				RequestAQuotePage.class);		
		HomePage homePage = PageFactory.initElements(getWebDriver(), HomePage.class);
		
		requestQuoteModel.setCommentText(super.getComment("Test ESP Mobile RAQ Build & Price"));
		requestQuoteModel.setCampaignCode("tmtt29500000");
		requestQuoteModel.setSiteName("mobile");
		
		requestAQuotePage.submitZipCodeMobile(requestQuoteModel);
		reportLog("Submit zip from popup");
			
		homePage.selectEspanolLanguageMobile();
		reportLog("select espanol language");
		
		homePage.selectShoppingToolMenu();
		reportLog("click on shopping tool menu");
		BuildAndPricePage buildAndPricePage = homePage.gotoBuildAndPriceEspMobilePage();
		reportLog("Go to build and price page");
		
		buildAndPricePage.selectCarBySeriesEsp(requestQuoteModel);
		reportLog("Select series from car list "+requestQuoteModel.getSeriesName());
		
		buildAndPricePage.verifySelectedTab("model");
		buildAndPricePage.clickNextButton();
		reportLog("Select model tab");
		
		buildAndPricePage.verifySelectedTab("engien");
		buildAndPricePage.clickNextButton();
		reportLog("Select engien tab");
		
		buildAndPricePage.verifySelectedTab("color");
		buildAndPricePage.clickNextButton();
		reportLog("Select color tab");
		
		buildAndPricePage.verifySelectedTab("package");
		buildAndPricePage.clickNextButton();
		reportLog("Select package tab");
		
		buildAndPricePage.verifySelectedTab("accessory");
		buildAndPricePage.clickNextButton();		
		reportLog("Select accessory tab");
		
		buildAndPricePage.verifySelectedTab("summary");		
		requestAQuotePage = buildAndPricePage.clickRaqButton();
		reportLog("Select summary tab");
		
		requestQuoteModel = buildAndPricePage.getPriceEsp(requestQuoteModel);
		requestAQuotePage.fillRQDetail(requestQuoteModel);
		requestQuoteModel.setVendorName(requestAQuotePage.getVendorName());
		requestQuoteModel.setDealerCode(requestAQuotePage.dealerCode());
		reportLog("fill request detail " + requestQuoteModel.toString());

		String currentUrl = getWebDriver().getCurrentUrl();
		requestQuoteModel = Utilities.setColor(requestQuoteModel, currentUrl);
				
		//GmailAPI.delete(requestQuoteModel.getEmail(), gmailPass);
		requestAQuotePage.clickSubmitButton();
		reportLog("click on submit button");
		
		String message = requestAQuotePage.getMessage();
		Assert.assertEquals(message.trim(), GlobalPagesConstant.RequestSentEsp);
		reportLog("Verify message request send successfully");
		System.out.println(requestQuoteModel.toString());
		
		String raqConfirmationMessage = requestAQuotePage.getConfirmationThankYouMessage();
		String year = Utilities.getSeriesYearEsp(requestQuoteModel);
		String seriesName = Utilities.getSeriesNameEsp(requestQuoteModel);
		Assert.assertTrue(raqConfirmationMessage.trim().toLowerCase().contains(
				GlobalPagesConstant.ThankYouConfirmationEsp.toLowerCase() + " "
						+ year.toLowerCase() + " "+ seriesName.toLowerCase()));			
		reportLog("Verify message Thank you! A dealer will contact you soon with a quote on your new Vehicle.");

		requestAQuotePage.verifyBAndPButtonsAfterSubmitEsp();
		reportLog("Verify 'Search vender' and 'Continue your Build' buttons");
				
		String from = "tmsusaincsaleslead@tmsusaconnect.com";
		String mailContent = GmailAPI.check(requestQuoteModel.getEmail(), gmailPass, from,
				requestQuoteModel.getCommentText());
		reportLog("Read email content");
		reportLog(mailContent);
		//GmailAPI.delete(requestQuoteModel.getEmail(), gmailPass);
		//reportLog("Delete emails");

		Assert.assertNotEquals("Not found", mailContent, "Email not recieved in 2 minutes");
		ValidationData.validateRequestQuoteTextEmailData(requestQuoteModel, mailContent, "Esp");
		reportLog("Verify email content successfully"); 
	}
}