package com.test.espanol.desktop;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Guice;
import org.testng.annotations.Test;

import com.demo.constant.GlobalPagesConstant;
import com.demo.datamodel.RequestQuoteModel;
import com.demo.dataproviders.DataProvidersEsp;
import com.demo.gmail.GmailAPI;
import com.demo.pages.HomePage;
import com.demo.pages.LocalSpecialPage;
import com.demo.pages.RequestAQuotePage;
import com.demo.selenium.core.BaseTest;
import com.demo.utilities.Utilities;
import com.demo.validation.ValidationData;

@Guice
public class RAQLocalSpecialEsp extends BaseTest {
	
	
	@Test(dataProvider = "LocalSpecial", dataProviderClass=DataProvidersEsp.class)
	public void RAQLocalSpecialEspTest(RequestQuoteModel requestQuoteModel) throws Exception {
		RequestAQuotePage requestAQuotePage = PageFactory.initElements(getWebDriver(),
				RequestAQuotePage.class);		
		HomePage homePage = PageFactory.initElements(getWebDriver(), HomePage.class);
		
		requestQuoteModel.setCommentText(super.getComment("Test ESP Desktop RAQ Local Specials"));
		requestQuoteModel.setCampaignCode("TWTT12790000");
		requestQuoteModel.setSiteName("website");
		
		homePage.selectEspanolLanguage();
		reportLog("select espanol language");
		
		requestAQuotePage.submitZipCode(requestQuoteModel);
		reportLog("submit zip code");
		
		reportLog("click on shopping tool menu");		
		homePage.selectShoppingToolMenu();
		LocalSpecialPage localSpecialPage = homePage.gotoLocalSpecialEsp();		
		reportLog("Go to local special page");
		
		localSpecialPage.clickViewDetails(requestQuoteModel);
		requestQuoteModel.setSeriesName(Utilities.splitStringWithNewLine(
				localSpecialPage.getSeriesNameEsp())[0]);
		
		String name = localSpecialPage.getSeriesNameEsp();
		System.out.println("series Name" + name);
		requestAQuotePage = localSpecialPage.clickRequestAQuoteButtonEsp();
		reportLog("Click on view details button and click on request a quote button");
		
		requestAQuotePage.fillRQDetailForLocalSpecial(requestQuoteModel);
		requestQuoteModel.setVendorName(requestAQuotePage.getVendorName());
		requestQuoteModel.setDealerCode(requestAQuotePage.dealerCode());
		reportLog("fill request detail " + requestQuoteModel.toString());

		//GmailAPI.delete(requestQuoteModel.getEmail(), gmailPass);
		requestAQuotePage.clickSubmitButton();
		reportLog("click on submit button");

		String message = requestAQuotePage.getMessage();
		Assert.assertEquals(message.trim(), GlobalPagesConstant.RequestSentEsp);
		reportLog("Verify message request send successfully");
		System.out.println(requestQuoteModel.toString());
		
		String raqConfirmationMessage = requestAQuotePage.getConfirmationThankYouMessage();
		Assert.assertTrue(raqConfirmationMessage.trim().toLowerCase().contains(
				GlobalPagesConstant.ThankYouConfirmationEsp.toLowerCase() + " "
						+ requestQuoteModel.getSeriesName().toLowerCase()));		
		reportLog("Verify message Thank you! A dealer will contact you soon with a quote on your new Vehicle.");

		requestAQuotePage.verifyButtonsEsp();
		reportLog("Verify 'Search vender' and 'Build your own' buttons");
		
		String from = "tmsusaincsaleslead@tmsusaconnect.com";
		String mailContent = GmailAPI.check(requestQuoteModel.getEmail(), gmailPass, from,
				requestQuoteModel.getCommentText());
		reportLog("Read email content");
		reportLog(mailContent);
		//GmailAPI.delete(requestQuoteModel.getEmail(), gmailPass);
		//reportLog("Delete emails");

		Assert.assertNotEquals("Not found", mailContent, "Email not recieved in 2 minutes");
		ValidationData.validateRequestQuoteTextEmailData(requestQuoteModel, mailContent, "Default");
		reportLog("Verify email content successfully"); 
	}

}
