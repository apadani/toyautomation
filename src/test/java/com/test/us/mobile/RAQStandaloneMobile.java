/*
 * 
 */
package com.test.us.mobile;

import org.testng.annotations.Test;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Guice;
import com.demo.constant.GlobalPagesConstant;
import com.demo.datamodel.RequestQuoteModel;
import com.demo.dataproviders.DataProvidersUs;
import com.demo.gmail.GmailAPI;
import com.demo.pages.HomePage;
import com.demo.pages.RequestAQuotePage;
import com.demo.selenium.core.BaseTest;
import com.demo.utilities.Utilities;
import com.demo.validation.ValidationData;

@Guice
public class RAQStandaloneMobile extends BaseTest {

	public RAQStandaloneMobile(){
		mobileTest = true;
	}
	
	@Test(dataProvider = "Standalone", dataProviderClass = DataProvidersUs.class)
	public void RAQStandAloneMobileTest(RequestQuoteModel requestQuoteModel) throws Exception {

		HomePage homePage = PageFactory.initElements(getWebDriver(), HomePage.class);
		RequestAQuotePage requestAQuotePage = PageFactory.initElements(getWebDriver(), RequestAQuotePage.class);
		
		requestAQuotePage.submitZipCodeMobile(requestQuoteModel);
		reportLog("submit zip code by click from top menu");
		
		requestQuoteModel.setCommentText(super.getComment("TestUs Mobile RAQ Standalone"));
		requestQuoteModel.setCampaignCode("tmtt00310000");
		requestQuoteModel.setSiteName("mobile");

		requestAQuotePage = homePage.gotoRequestQuoteMobile();
		reportLog("click on request a quote menu");

		requestAQuotePage.fillRequestQuoteDetail(requestQuoteModel);
		requestQuoteModel.setVendorName(requestAQuotePage.getVendorName());

		requestQuoteModel.setDealerCode(requestAQuotePage.dealerCode());
		reportLog("fill request detail " + requestQuoteModel.toString());

		//GmailAPI.delete(requestQuoteModel.getEmail(), gmailPass);
		requestAQuotePage.clickSubmitButton();
		reportLog("click on submit button");

		String message = requestAQuotePage.getMessage();
		Assert.assertEquals(message.trim(), GlobalPagesConstant.RequestSent);
		reportLog("Verify message request send successfully");

		String raqConfirmationMessage = requestAQuotePage.getConfirmationThankYouMessage();

		Assert.assertTrue(
				raqConfirmationMessage.trim().toLowerCase()
						.contains(GlobalPagesConstant.ThankYouConfirmation.toLowerCase() + " "
								+ requestQuoteModel.getSeriesName().toLowerCase()),
				"Thank you message Verification failed");

		reportLog("Verify message Thank you! A dealer will contact you soon with a quote on your new Vehicle.");

		requestAQuotePage.verifyButtons();
		reportLog("Verify 'Search vender' and 'Build your own' buttons");

		String from = "tmsusaincsaleslead@tmsusaconnect.com";
		String mailContent = GmailAPI.check(requestQuoteModel.getEmail(), gmailPass, from,
				requestQuoteModel.getCommentText());
		reportLog("Read email content");
		reportLog(mailContent);
		//GmailAPI.delete(requestQuoteModel.getEmail(), gmailPass);
		reportLog("Delete emails");

		Assert.assertNotEquals("Not found", mailContent, "Email not recieved in 2 minutes");
		ValidationData.validateRequestQuoteTextEmailData(requestQuoteModel, mailContent, "US");
		reportLog("Verify email content successfully");
	}

}
