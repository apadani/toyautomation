package com.test.us.mobile;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Guice;
import com.demo.constant.GlobalPagesConstant;
import com.demo.datamodel.RequestQuoteModel;
import com.demo.dataproviders.DataProvidersUs;
import com.demo.gmail.GmailAPI;
import com.demo.pages.BuildAndPricePage;
import com.demo.pages.HomePage;
import com.demo.pages.RequestAQuotePage;
import com.demo.selenium.core.BaseTest;
import com.demo.utilities.Utilities;
import com.demo.validation.ValidationData;

@Guice
public class RAQBuildAndPriceMobile extends BaseTest {

	public RAQBuildAndPriceMobile(){
		mobileTest = true;
	}
	
	@Test(dataProvider = "BuildAndPrice", dataProviderClass = DataProvidersUs.class)
	public void RAQBuildAndPriceMobileTest(RequestQuoteModel requestQuoteModel) throws Exception {

		RequestAQuotePage requestAQuotePage = PageFactory.initElements(getWebDriver(), RequestAQuotePage.class);

		requestQuoteModel.setCommentText(super.getComment("TestUs Mobile RAQ Build & Price"));
		requestQuoteModel.setCampaignCode("tmtt03700000");
		requestQuoteModel.setSiteName("mobile");
		
		HomePage homePage = PageFactory.initElements(getWebDriver(), HomePage.class);
		requestAQuotePage.submitZipCodeMobile(requestQuoteModel);
		reportLog("submit zip code");

		BuildAndPricePage buildAndPricePage = homePage.gotoBuildAndPriceMobilePage();
		reportLog("Go to build and price page");

		buildAndPricePage.selectCarBySeries(requestQuoteModel);
		reportLog("Select series from car list " + requestQuoteModel.getSeriesName());

		buildAndPricePage.verifySelectedTab("model");
		buildAndPricePage.clickNextButton();
		reportLog("Select model tab");

		buildAndPricePage.verifySelectedTab("engien");
		buildAndPricePage.clickNextButton();
		reportLog("Select engien tab");

		buildAndPricePage.verifySelectedTab("color");
		buildAndPricePage.clickNextButton();
		reportLog("Select color tab");

		buildAndPricePage.verifySelectedTab("package");
		buildAndPricePage.clickNextButton();
		reportLog("Select package tab");

		buildAndPricePage.verifySelectedTab("accessory");
		buildAndPricePage.clickNextButton();
		reportLog("Select accessory tab");

		buildAndPricePage.verifySelectedTab("summary");
		requestAQuotePage = buildAndPricePage.clickRaqButton();
		reportLog("Select summary tab");

		requestQuoteModel = buildAndPricePage.getPrice(requestQuoteModel);
		requestAQuotePage.fillRQDetail(requestQuoteModel);
		requestQuoteModel.setVendorName(requestAQuotePage.getVendorName());
		requestQuoteModel.setDealerCode(requestAQuotePage.dealerCode());
		reportLog("fill request detail " + requestQuoteModel.toString());

		String currentUrl = getWebDriver().getCurrentUrl();
		requestQuoteModel = Utilities.setColor(requestQuoteModel, currentUrl);

		//GmailAPI.delete(requestQuoteModel.getEmail(), gmailPass);
		requestAQuotePage.clickSubmitButton();
		reportLog("click on submit button");

		String message = requestAQuotePage.getMessage();
		AssertJUnit.assertEquals(message.trim(), GlobalPagesConstant.RequestSent);
		reportLog("Verify message request send successfully");
		System.out.println(requestQuoteModel.toString());

		String raqConfirmationMessage = requestAQuotePage.getConfirmationThankYouMessage();
		System.out.println(raqConfirmationMessage);
		System.out.println(requestQuoteModel.getSeriesName());

		Assert.assertTrue(raqConfirmationMessage.trim().toLowerCase().contains(GlobalPagesConstant.ThankYouConfirmation.toLowerCase() 
				+ " " + requestQuoteModel.getSeriesName().toLowerCase()),
				"Thank you message Verification failed");
		reportLog("Verify message Thank you! A dealer will contact you soon with a quote on your new Vehicle.");
		System.out.println(requestQuoteModel.toString());

		requestAQuotePage.verifyBAndPButtonsAfterSubmit();
		reportLog("Verify 'Search vender' and 'Continue your Build' buttons");

		String from = "tmsusaincsaleslead@tmsusaconnect.com";
		String mailContent = GmailAPI.check(requestQuoteModel.getEmail(), gmailPass, from, 
				requestQuoteModel.getCommentText());
		reportLog("Read email content");
		reportLog(mailContent);
		//GmailAPI.delete(requestQuoteModel.getEmail(), gmailPass);
		reportLog("Delete emails");

		Assert.assertNotEquals("Not found", mailContent, "Email not recieved in 2 minutes");
		ValidationData.validateRequestQuoteTextEmailData(requestQuoteModel, mailContent, "US");
		reportLog("Verify email content successfully");
	}
}
