package com.test.us.desktop;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Guice;
import org.testng.annotations.Test;

import com.demo.constant.GlobalPagesConstant;
import com.demo.datamodel.RequestQuoteModel;
import com.demo.dataproviders.DataProvidersUs;
import com.demo.gmail.GmailAPI;
import com.demo.pages.HomePage;
import com.demo.pages.LocalSpecialPage;
import com.demo.pages.RequestAQuotePage;
import com.demo.selenium.core.BaseTest;
import com.demo.utilities.Utilities;
import com.demo.validation.ValidationData;

@Guice
public class RAQLocalSpecial extends BaseTest {

	@Test(dataProvider = "LocalSpecial", dataProviderClass = DataProvidersUs.class)
	public void RAQLocalSpecialTest(RequestQuoteModel requestQuoteModel) throws Exception {
		RequestAQuotePage requestAQuotePage = PageFactory.initElements(getWebDriver(), RequestAQuotePage.class);

		requestQuoteModel.setCommentText(super.getComment("TestUS desktop RAQ Local Specials")); 
		requestQuoteModel.setCampaignCode("twtt12870000");
		requestQuoteModel.setSiteName("website");
		
		requestAQuotePage.submitZipCode(requestQuoteModel);
		reportLog("submit zip code");

		reportLog("click on shopping tool menu");
		HomePage homePage = PageFactory.initElements(getWebDriver(), HomePage.class);
		homePage.selectShoppingToolMenu();
		LocalSpecialPage localSpecialPage = homePage.gotoLocalSpecial();
		reportLog("Go to local special page");

		localSpecialPage.clickViewDetails(requestQuoteModel);
		requestQuoteModel.setSeriesName(Utilities.splitStringWithNewLine(localSpecialPage.getSeriesName())[0]);

		String name = localSpecialPage.getSeriesName();
		System.out.println("series Name" + name);
		requestAQuotePage = localSpecialPage.clickRequestAQuoteButton();
		reportLog("Click on view details button and click on request a quote button");
		Thread.sleep(2000);

		requestAQuotePage.fillRQDetailForLocalSpecial(requestQuoteModel);
		requestQuoteModel.setVendorName(requestAQuotePage.getVendorNameLocalSpecial());
		requestQuoteModel.setDealerCode(requestAQuotePage.dealerCodeLocalSpecial());
		reportLog("fill request detail " + requestQuoteModel.toString());

		//GmailAPI.delete(requestQuoteModel.getEmail(), gmailPass);
		requestAQuotePage.clickRAQLocalSpecialSubmit();
		reportLog("click on submit button");

		String message = requestAQuotePage.getMessage();
		Assert.assertEquals(message.trim(), GlobalPagesConstant.RequestSent);
		reportLog("Verify message request send successfully");
		System.out.println(requestQuoteModel.toString());

		String raqConfirmationMessage = requestAQuotePage.getConfirmationThankYouMessage();
		System.out.println(raqConfirmationMessage + "test");
		Assert.assertTrue(raqConfirmationMessage.trim().toLowerCase()
				.contains(GlobalPagesConstant.ThankYouConfirmation.toLowerCase()));
		reportLog("Verify message Thank you! A dealer will contact you soon with a quote on your new Vehicle.");

		requestAQuotePage.verifyButtons();
		reportLog("Verify 'Search vender' and 'Build your own' buttons");

		String from = "tmsusaincsaleslead@tmsusaconnect.com";
		String mailContent = GmailAPI.check(requestQuoteModel.getEmail(), gmailPass, from,
				requestQuoteModel.getCommentText());
		reportLog("Read email content");
		reportLog(mailContent);
		//GmailAPI.delete(requestQuoteModel.getEmail(), gmailPass);
		//reportLog("Delete emails");

		Assert.assertNotEquals("Not found", mailContent, "Email not recieved in 2 minutes");
		ValidationData.validateRequestQuoteTextEmailData(requestQuoteModel, mailContent, "US");
		reportLog("Verify email content successfully");
	}

}