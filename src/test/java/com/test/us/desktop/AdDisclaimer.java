package com.test.us.desktop;

import java.util.List;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Guice;
import org.testng.annotations.Test;

import com.demo.dataproviders.DataProvidersUs;
import com.demo.pages.DisclaimerPage;
import com.demo.selenium.core.BaseTest;

@Guice
public class AdDisclaimer extends BaseTest {
	
	@Test(dataProvider = "Disclaimer", dataProviderClass = DataProvidersUs.class)
	public void testDisclaimer(String name, String uri, String disclaimerText) throws Exception{
		getWebDriver().navigate().to(super.applicationUrl + "/"+uri);
		DisclaimerPage disclaimerPage = PageFactory.initElements(getWebDriver(), DisclaimerPage.class);
		List<String> disclaimerList = disclaimerPage.getDisclaimerList();		
		String[] str = disclaimerText.split("\n");	
		for(String str1 : str) {
			Assert.assertTrue(disclaimerList.contains(str1), "Uri "+ uri + "  not contains disclaimer "+str1);
		}			
		
	}
}
