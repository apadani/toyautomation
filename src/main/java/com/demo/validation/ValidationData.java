package com.demo.validation;

import org.testng.Assert;
import com.demo.datamodel.RequestQuoteModel;
import com.demo.utilities.Utilities;

public class ValidationData {

	/**
	 * Validate email content of request a quote
	 * 
	 * @param requestQuoteModel
	 *            : request data hold
	 * @param mailContent
	 *            : mail content
	 */
	public static void validateRequestQuoteTextEmailData(RequestQuoteModel requestQuoteModel, String mailContent, String language) {
		//Assert.assertTrue(mailContent.contains("<b>year:</b>" + requestQuoteModel.getYear() + "</p>"), requestQuoteModel.getYear() + " Year not found in Email");
		Assert.assertTrue(mailContent.toLowerCase().contains("<b>make:</b>" + requestQuoteModel.getMake().toLowerCase() + "</p>"), requestQuoteModel.getMake() + " Make not found in Email");
	
		switch (language) {
		case "Esp":
			Assert.assertTrue(mailContent.contains("<b>year:</b>" + Utilities.getSeriesYearEsp(requestQuoteModel) + "</p>"), Utilities.getSeriesYearEsp(requestQuoteModel)+ " Year not found in Email");
			Assert.assertTrue(mailContent.toLowerCase().contains("<b>model:</b>" + Utilities.getSeriesNameEsp(requestQuoteModel).toLowerCase() + "</p>"),
					Utilities.getSeriesNameEsp(requestQuoteModel).toLowerCase() + "Model not found in Email");
			break;
		default:
			Assert.assertTrue(mailContent.contains("<b>year:</b>" + requestQuoteModel.getYear() + "</p>"), requestQuoteModel.getYear() + " Year not found in Email");
			Assert.assertTrue(mailContent.toLowerCase().contains("<b>model:</b>" + requestQuoteModel.getModel().toLowerCase() + "</p>"), requestQuoteModel.getModel() + "Model not found in Email");
			break;
		}
		//Assert.assertTrue(mailContent.toLowerCase().contains("<b>model:</b>" + requestQuoteModel.getModel().toLowerCase() + "</p>"), requestQuoteModel.getModel() + "Model not found in Email");
		
		//if (requestQuoteModel.getTrim() != null && !requestQuoteModel.getTrim().equals("")) {
			//Assert.assertTrue(mailContent.toLowerCase().contains("<b>trim:</b>" + requestQuoteModel.getTrim().toLowerCase() + "</p>"), requestQuoteModel.getTrim() + " Trim not found in Email");
		//}
		Assert.assertTrue(mailContent.contains("<b>first name:</b>" + requestQuoteModel.getFirstName().toLowerCase() + "</p>"), requestQuoteModel.getFirstName() + " First Name not found in Email");
		Assert.assertTrue(mailContent.contains("<b>last name:</b>" + requestQuoteModel.getLastName().toLowerCase() + "</p>"), requestQuoteModel.getLastName() + " Last name not found in Email");
		Assert.assertTrue(mailContent.contains("<b>address line1:</b>" + requestQuoteModel.getAddress().toLowerCase() + "</p>"), requestQuoteModel.getAddress() + " Address not found in Email");
		Assert.assertTrue(mailContent.contains("<b>email:</b>" + requestQuoteModel.getEmail().toLowerCase() + "</p>"), requestQuoteModel.getEmail() + " Mail not found in Email");
		Assert.assertTrue(mailContent.contains("<b>city:</b>" + requestQuoteModel.getCity().toLowerCase() + "</p>"), requestQuoteModel.getCity() + " City not found in Email");
		String phoneNumber = requestQuoteModel.getPhone().replaceAll("[^0-9]", "");
		Assert.assertTrue(mailContent.contains("<b>phone:</b>" + phoneNumber + "</p>"), requestQuoteModel.getPhone() + " Phone nuumber not found in Email");
		if (requestQuoteModel.getExteriorColor() != null && !requestQuoteModel.getExteriorColor().equals(""))
			Assert.assertTrue(mailContent.toLowerCase().contains("<b>exterior color:</b>" + requestQuoteModel.getExteriorColor().toLowerCase() + "</p>"),
					requestQuoteModel.getExteriorColor() + " Exterior Color not found in Email");
		if (requestQuoteModel.getInteriorColor() != null && !requestQuoteModel.getInteriorColor().equals(""))
			Assert.assertTrue(mailContent.toLowerCase().contains("<b>interior color:</b>" + requestQuoteModel.getInteriorColor().toLowerCase() + "</p>"),
					requestQuoteModel.getInteriorColor() + " Interior Color not found in Email");
		if (requestQuoteModel.getPrice() != null && !requestQuoteModel.getPrice().equals(""))
			Assert.assertTrue(mailContent.toLowerCase().contains("<b>vehicle price:</b>" + requestQuoteModel.getPrice().toLowerCase() + "</p>"),
					requestQuoteModel.getPrice() + " price not found in Email");		
		Assert.assertTrue(mailContent.contains( requestQuoteModel.getVendorName().toLowerCase()),
				requestQuoteModel.getVendorName() + " Vendor Name Not found in Email");
		Assert.assertTrue(mailContent.contains(requestQuoteModel.getDealerCode().toLowerCase() + "</p>"), requestQuoteModel.getDealerCode() + " Dealer code not found in Email");
		Assert.assertTrue(mailContent.toLowerCase().contains("<b>lead source:</b>"+requestQuoteModel.getSiteName()+"</p>"), " Web site for Website not found in Email");
		if (requestQuoteModel.getOfferRibbon() != null && !requestQuoteModel.getOfferRibbon().equals("")) {
			if (requestQuoteModel.getOfferRibbon().toLowerCase().equals("finance") || requestQuoteModel.getOfferRibbon().toLowerCase().equals("financiamiento")) {
				Assert.assertTrue(mailContent.contains("type: apr"), "apr not found for finance in Email");
			} else {
				Assert.assertTrue(mailContent.toLowerCase().contains("type: lease"), requestQuoteModel.getOfferRibbon() + " not found in Email");
			}
		}
		Assert.assertTrue(mailContent.toLowerCase().contains("<b>lead sub source:</b>" + requestQuoteModel.getCampaignCode().toLowerCase() + "</p>"), "Campaign code not found in Email");
		Assert.assertTrue(mailContent.contains("<b>postal code:</b>" + requestQuoteModel.getZipCode() + "</p>"), requestQuoteModel.getZipCode() + " Zip not found in Email");
	}

	/**
	 * Validate email content of request a quote
	 * 
	 * @param requestQuoteModel
	 *            : request data hold
	 * @param mailContent
	 *            : mail content
	 */
	public static void validateRequestQuoteEmailData(RequestQuoteModel requestQuoteModel, String mailContent) {
		Assert.assertTrue(mailContent.contains("<year>" + requestQuoteModel.getYear() + "</year>"), requestQuoteModel.getYear() + " Year not found in Email");
		Assert.assertTrue(mailContent.toLowerCase().contains("<make>" + requestQuoteModel.getMake().toLowerCase() + "</make>"), requestQuoteModel.getMake() + " Make not found in Email");
		Assert.assertTrue(mailContent.toLowerCase().contains("<model>" + requestQuoteModel.getModel().toLowerCase() + "</model>"), requestQuoteModel.getModel() + "Model not found in Email");
		Assert.assertTrue(mailContent.toLowerCase().contains("<trim>" + requestQuoteModel.getTrim().toLowerCase() + "</trim>"), requestQuoteModel.getTrim() + " Trim not found in Email");
		Assert.assertTrue(mailContent.contains("<name part=\"first\" type=\"individual\">" + requestQuoteModel.getFirstName().toLowerCase() + "</name>"),
				requestQuoteModel.getFirstName() + " First Name not found in Email");
		Assert.assertTrue(mailContent.contains("<name part=\"last\" type=\"individual\">" + requestQuoteModel.getLastName().toLowerCase() + "</name>"),
				requestQuoteModel.getLastName() + " Last name not found in Email");
		Assert.assertTrue(mailContent.contains("<street>" + requestQuoteModel.getAddress().toLowerCase() + "</street>"), requestQuoteModel.getAddress() + " Address not found in Email");
		Assert.assertTrue(mailContent.contains("<email preferredcontact=\"1\">" + requestQuoteModel.getEmail().toLowerCase() + "</email>"), requestQuoteModel.getEmail() + " Mail not found in Email");
		Assert.assertTrue(mailContent.contains("<city>" + requestQuoteModel.getCity().toLowerCase() + "</city>"), requestQuoteModel.getCity() + " City not found in Email");
		Assert.assertTrue(mailContent.contains("<phone type=\"voice\" time=\"nopreference\" preferredcontact=\"0\">" + requestQuoteModel.getPhone() + "</phone>"),
				requestQuoteModel.getPhone() + " Phone nuumber not found in Email");
		Assert.assertTrue(mailContent.contains("<postalcode>" + requestQuoteModel.getZipCode() + "</postalcode>"), requestQuoteModel.getZipCode() + " Zip not found in Email");
		Assert.assertTrue(mailContent.contains("<vendorname>" + requestQuoteModel.getVendorName().toLowerCase() + "</vendorname>"),
				requestQuoteModel.getVendorName() + " Vendor Name Not found in Email");
		Assert.assertTrue(mailContent.contains("<id sequence=\"1\" source=\"tms dealer code\">" + requestQuoteModel.getDealerCode().toLowerCase() + "</id>"),
				requestQuoteModel.getDealerCode() + " Dealer code not found in Email");
		Assert.assertTrue(mailContent.contains("<name>" + requestQuoteModel.getSiteName().toLowerCase() + "</name>"), " Web site for " + requestQuoteModel.getSiteName() + " not found in Email");
		Assert.assertTrue(mailContent.toLowerCase().contains("<service>" + requestQuoteModel.getCampaignCode().toLowerCase() + "</service>"), "Campaign code not found in Email");
	}

	/**
	 * Validate email content of request a quote
	 * 
	 * @param requestQuoteModel
	 *            : request data hold
	 * @param mailContent
	 *            : mail content
	 */
	public static void validateBuildAndPriceRQEmailData(RequestQuoteModel requestQuoteModel, String mailContent, String language) {

		Assert.assertTrue(mailContent.toLowerCase().contains("<make>" + requestQuoteModel.getMake().toLowerCase() + "</make>"), requestQuoteModel.getMake() + " Make not found in Email");
		switch (language) {
		case "Esp":
			Assert.assertTrue(mailContent.contains("<year>" + Utilities.getSeriesYearEsp(requestQuoteModel).toLowerCase() + "</year>"), requestQuoteModel.getYear() + " Year not found in Email");
			Assert.assertTrue(mailContent.toLowerCase().contains("<model>" + Utilities.getSeriesNameEsp(requestQuoteModel).toLowerCase() + "</model>"),
					requestQuoteModel.getModel() + "Model not found in Email");
			break;
		default:
			Assert.assertTrue(mailContent.contains("<year>" + requestQuoteModel.getYear() + "</year>"), requestQuoteModel.getYear() + " Year not found in Email");
			Assert.assertTrue(mailContent.toLowerCase().contains("<model>" + requestQuoteModel.getModel().toLowerCase() + "</model>"), requestQuoteModel.getModel() + "Model not found in Email");
			break;
		}
		Assert.assertTrue(mailContent.toLowerCase().contains("<exteriorcolor>" + requestQuoteModel.getExteriorColor().toLowerCase() + "</exteriorcolor>"),
				requestQuoteModel.getExteriorColor() + " Exterior Color not found in Email");
		Assert.assertTrue(mailContent.toLowerCase().contains("<interiorcolor>" + requestQuoteModel.getInteriorColor().toLowerCase() + "</interiorcolor>"),
				requestQuoteModel.getInteriorColor() + " Interior Color not found in Email");
		Assert.assertTrue(mailContent.toLowerCase().contains("<price currency=\"usd\">" + requestQuoteModel.getPrice().toLowerCase() + "</price>"),
				requestQuoteModel.getPrice() + " price not found in Email");
		Assert.assertTrue(mailContent.contains("<name part=\"first\" type=\"individual\">" + requestQuoteModel.getFirstName().toLowerCase() + "</name>"),
				requestQuoteModel.getFirstName() + " First Name not found in Email");
		Assert.assertTrue(mailContent.contains("<name part=\"last\" type=\"individual\">" + requestQuoteModel.getLastName().toLowerCase() + "</name>"),
				requestQuoteModel.getLastName() + " Last name not found in Email");
		Assert.assertTrue(mailContent.contains("<street>" + requestQuoteModel.getAddress().toLowerCase() + "</street>"), requestQuoteModel.getAddress() + " Address not found in Email");
		Assert.assertTrue(mailContent.contains("<email preferredcontact=\"1\">" + requestQuoteModel.getEmail().toLowerCase() + "</email>"), requestQuoteModel.getEmail() + " Mail not found in Email");
		Assert.assertTrue(mailContent.contains("<city>" + requestQuoteModel.getCity().toLowerCase() + "</city>"), requestQuoteModel.getCity() + " City not found in Email");
		Assert.assertTrue(mailContent.contains("<phone type=\"voice\" time=\"nopreference\" preferredcontact=\"0\">" + requestQuoteModel.getPhone() + "</phone>"),
				requestQuoteModel.getPhone() + " Phone nuumber not found in Email");
		Assert.assertTrue(mailContent.contains("<postalcode>" + requestQuoteModel.getZipCode() + "</postalcode>"), requestQuoteModel.getZipCode() + " Zip not found in Email");
		Assert.assertTrue(mailContent.contains("<vendorname>" + requestQuoteModel.getVendorName().toLowerCase() + "</vendorname>"),
				requestQuoteModel.getVendorName() + " Vendor Name Not found in Email");
		Assert.assertTrue(mailContent.contains("<id sequence=\"1\" source=\"tms dealer code\">" + requestQuoteModel.getDealerCode().toLowerCase() + "</id>"),
				requestQuoteModel.getDealerCode() + " Dealer code not found in Email");
		Assert.assertTrue(mailContent.contains("<name>" + requestQuoteModel.getSiteName().toLowerCase() + "</name>"), " Web site for " + requestQuoteModel.getSiteName() + " not found in Email");
		Assert.assertTrue(mailContent.toLowerCase().contains("<service>" + requestQuoteModel.getCampaignCode().toLowerCase() + "</service>"), "Campaign code not found in Email");
	}

	/**
	 * Validate email content of request a quote
	 * 
	 * @param requestQuoteModel
	 *            : request data hold
	 * @param mailContent
	 *            : mail content
	 */
	public static void validateLocalSpecialEmailData(RequestQuoteModel requestQuoteModel, String mailContent) {
		Assert.assertTrue(mailContent.contains("<year>" + requestQuoteModel.getYear() + "</year>"), requestQuoteModel.getYear() + " Year not found in Email");
		Assert.assertTrue(mailContent.toLowerCase().contains("<make>" + requestQuoteModel.getMake().toLowerCase() + "</make>"), requestQuoteModel.getMake() + " Make not found in Email");
		Assert.assertTrue(mailContent.toLowerCase().contains("<model>" + requestQuoteModel.getModel().toLowerCase() + "</model>"), requestQuoteModel.getModel() + " Model not found in Email");
		Assert.assertTrue(mailContent.contains("<name part=\"first\" type=\"individual\">" + requestQuoteModel.getFirstName().toLowerCase() + "</name>"),
				requestQuoteModel.getFirstName() + " First Name not found in Email");
		Assert.assertTrue(mailContent.contains("<name part=\"last\" type=\"individual\">" + requestQuoteModel.getLastName().toLowerCase() + "</name>"),
				requestQuoteModel.getLastName() + " Last name not found in Email");
		Assert.assertTrue(mailContent.contains("<street>" + requestQuoteModel.getAddress().toLowerCase() + "</street>"), requestQuoteModel.getAddress() + " Address not found in Email");
		Assert.assertTrue(mailContent.contains("<email preferredcontact=\"1\">" + requestQuoteModel.getEmail().toLowerCase() + "</email>"), requestQuoteModel.getEmail() + " Mail not found in Email");
		Assert.assertTrue(mailContent.contains("<city>" + requestQuoteModel.getCity().toLowerCase() + "</city>"), requestQuoteModel.getCity() + " City not found in Email");
		Assert.assertTrue(mailContent.contains("<phone type=\"voice\" time=\"nopreference\" preferredcontact=\"0\">" + requestQuoteModel.getPhone() + "</phone>"),
				requestQuoteModel.getPhone() + " Phone nuumber not found in Email");
		Assert.assertTrue(mailContent.contains("<postalcode>" + requestQuoteModel.getZipCode() + "</postalcode>"), requestQuoteModel.getZipCode() + " Zip not found in Email");
		Assert.assertTrue(mailContent.contains("<vendorname>" + requestQuoteModel.getVendorName().toLowerCase() + "</vendorname>"),
				requestQuoteModel.getVendorName() + " Vendor Name Not found in Email");
		Assert.assertTrue(mailContent.contains("<id sequence=\"1\" source=\"tms dealer code\">" + requestQuoteModel.getDealerCode().toLowerCase() + "</id>"),
				requestQuoteModel.getDealerCode() + " Dealer code not found in Email");
		if (requestQuoteModel.getOfferRibbon().toLowerCase().equals("finance") || requestQuoteModel.getOfferRibbon().toLowerCase().equals("financiamiento")) {
			Assert.assertTrue(mailContent.contains("type: apr"), "apr not found for finance in Email");
		} else {
			Assert.assertTrue(mailContent.toLowerCase().contains("type: " + "lease"), requestQuoteModel.getOfferRibbon() + " not found in Email");
		}
		Assert.assertTrue(mailContent.contains("<name>" + requestQuoteModel.getSiteName().toLowerCase() + "</name>"), " Web site for " + requestQuoteModel.getSiteName() + " not found in Email");
		Assert.assertTrue(mailContent.toLowerCase().contains(requestQuoteModel.getCampaignCode()), "service not fond in mail");
	}

	/**
	 * Validate email content of request a quote
	 * 
	 * @param requestQuoteModel
	 *            : request data hold
	 * @param mailContent
	 *            : mail content
	 */
	public static void validateSearchInventoryEmailData(RequestQuoteModel requestQuoteModel, String mailContent, String language) {

		switch (language) {
		case "Esp":
			Assert.assertTrue(mailContent.contains("<year>" + Utilities.getSeriesYearEsp(requestQuoteModel).toLowerCase() + "</year>"), requestQuoteModel.getYear() + " Year not found in Email");
			Assert.assertTrue(mailContent.toLowerCase().contains("<model>" + Utilities.getSeriesNameEsp(requestQuoteModel).toLowerCase() + "</model>"),
					requestQuoteModel.getModel() + "Model not found in Email");
			break;
		default:
			Assert.assertTrue(mailContent.contains("<year>" + requestQuoteModel.getYear() + "</year>"), requestQuoteModel.getYear() + " Year not found in Email");
			Assert.assertTrue(mailContent.toLowerCase().contains("<model>" + requestQuoteModel.getModel().toLowerCase() + "</model>"), requestQuoteModel.getModel() + "Model not found in Email");
			break;
		}
		Assert.assertTrue(mailContent.toLowerCase().contains("<make>" + requestQuoteModel.getMake().toLowerCase() + "</make>"), requestQuoteModel.getMake() + " Make not found in Email");
		Assert.assertTrue(mailContent.toLowerCase().contains("<exteriorcolor>" + requestQuoteModel.getExteriorColor().toLowerCase() + "</exteriorcolor>"),
				requestQuoteModel.getExteriorColor() + " Exterior Color not found in Email");
		Assert.assertTrue(mailContent.toLowerCase().contains("<interiorcolor>" + requestQuoteModel.getInteriorColor().toLowerCase() + "</interiorcolor>"),
				requestQuoteModel.getInteriorColor() + " Interior Color not found in Email");
		Assert.assertTrue(mailContent.toLowerCase().contains("<price currency=\"usd\">" + requestQuoteModel.getPrice().toLowerCase() + "</price>"),
				requestQuoteModel.getPrice() + " price not found in Email");
		Assert.assertTrue(mailContent.contains("<name part=\"first\" type=\"individual\">" + requestQuoteModel.getFirstName().toLowerCase() + "</name>"),
				requestQuoteModel.getFirstName() + " First Name not found in Email");
		Assert.assertTrue(mailContent.contains("<name part=\"last\" type=\"individual\">" + requestQuoteModel.getLastName().toLowerCase() + "</name>"),
				requestQuoteModel.getLastName() + " Last name not found in Email");
		Assert.assertTrue(mailContent.contains("<street>" + requestQuoteModel.getAddress().toLowerCase() + "</street>"), requestQuoteModel.getAddress() + " Address not found in Email");
		Assert.assertTrue(mailContent.contains("<email preferredcontact=\"1\">" + requestQuoteModel.getEmail().toLowerCase() + "</email>"), requestQuoteModel.getEmail() + " Mail not found in Email");
		Assert.assertTrue(mailContent.contains("<city>" + requestQuoteModel.getCity().toLowerCase() + "</city>"), requestQuoteModel.getCity() + " City not found in Email");
		Assert.assertTrue(mailContent.contains("<phone type=\"voice\" time=\"nopreference\" preferredcontact=\"0\">" + requestQuoteModel.getPhone() + "</phone>"),
				requestQuoteModel.getPhone() + " Phone nuumber not found in Email");
		Assert.assertTrue(mailContent.contains("<postalcode>" + requestQuoteModel.getZipCode() + "</postalcode>"), requestQuoteModel.getZipCode() + " Zip not found in Email");
		Assert.assertTrue(mailContent.contains("<vendorname>" + requestQuoteModel.getVendorName().toLowerCase() + "</vendorname>"),
				requestQuoteModel.getVendorName() + " Vendor Name Not found in Email");
		Assert.assertTrue(mailContent.contains("<id sequence=\"1\" source=\"tms dealer code\">" + requestQuoteModel.getDealerCode().toLowerCase() + "</id>"),
				requestQuoteModel.getDealerCode() + " Dealer code not found in Email");
		Assert.assertTrue(mailContent.contains("<name>website-toyota.com</name>"), " Web site for toyota.com not found in Email");
		Assert.assertTrue(mailContent.toLowerCase().contains(requestQuoteModel.getCampaignCode()), "service not fond in mail");
	}

}
