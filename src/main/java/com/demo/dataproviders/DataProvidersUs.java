package com.demo.dataproviders;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.demo.datamodel.RequestQuoteModel;
import com.demo.utilities.DataBuilder;
import com.demo.utilities.Read_XLS;

/**
 * This class contains testng data provider which holds test data
 * 
 * @author Admin
 *
 */

public class DataProvidersUs {

	@DataProvider(name = "Standalone")
	public Iterator<Object[]> requestQuote() throws IOException {

		Collection<Object[]> requestData = new ArrayList<Object[]>();
		{
			{
				DataBuilder dataBuilder = new DataBuilder();
				List<RequestQuoteModel> data = dataBuilder.prepareRequestQuoteData();
				for (RequestQuoteModel requestQuoteModel : data) {
					requestData.add(new Object[] { requestQuoteModel });
				}
			}
		}

		return requestData.iterator();
	}

	/**
	 * Data Provider for build and price
	 * 
	 * @return
	 * @throws IOException
	 */
	@DataProvider(name = "BuildAndPrice")
	public Iterator<Object[]> requestBuildAndPrice() throws IOException {

		Collection<Object[]> requestData = new ArrayList<Object[]>();
		{
			{
				DataBuilder dataBuilder = new DataBuilder();
				List<RequestQuoteModel> data = dataBuilder.prepareBuildAndPriceData();
				for (RequestQuoteModel requestQuoteModel : data) {
					requestData.add(new Object[] { requestQuoteModel });
				}
			}
		}
		return requestData.iterator();
	}

	/**
	 * Data Provider for local special
	 * 
	 * @return
	 * @throws IOException
	 */
	@DataProvider(name = "LocalSpecial")
	public Iterator<Object[]> requestLocalSpecial() throws IOException {

		Collection<Object[]> requestData = new ArrayList<Object[]>();
		{
			{
				DataBuilder dataBuilder = new DataBuilder();
				List<RequestQuoteModel> data = dataBuilder.prepareLocalSpecialData();
				for (RequestQuoteModel requestQuoteModel : data) {
					requestData.add(new Object[] { requestQuoteModel });
				}
			}
		}
		return requestData.iterator();
	}

	/**
	 * Data Provider for search inventory
	 * 
	 * @return
	 * @throws IOException
	 */
	@DataProvider(name = "SearchInventory")
	public Iterator<Object[]> requestSearchInventory() throws IOException {

		Collection<Object[]> requestData = new ArrayList<Object[]>();
		{
			{
				DataBuilder dataBuilder = new DataBuilder();
				List<RequestQuoteModel> data = dataBuilder.prepareSearchInventoryData();
				for (RequestQuoteModel requestQuoteModel : data) {
					requestData.add(new Object[] { requestQuoteModel });
				}
			}
		}
		return requestData.iterator();
	}
	
	@DataProvider(name ="Disclaimer")
	public Object[][] disclaimerData(){
		
			Read_XLS read = new Read_XLS(DataBuilder.testDataSheet, "data/");
			Object[][] objs = new Object[read.retrieveNoOfRows("Discalimer") - 1][read.retrieveNoOfCols("Discalimer")];
			objs = read.retrieveTestData1("Discalimer");			
			return objs;
		
	}
	
	
	@Test(dataProvider = "Disclaimer", dataProviderClass = DataProvidersUs.class)
	public void testss(String name, String uri, String disclaimerText) {
		String[] str = disclaimerText.split("\n");
		System.out.println("=========================================================");
		for(String str1 : str){
			System.out.println(str1);
			System.out.println("");
		}
		//System.out.println(name +  "    " + uri +  "           "+disclaimerText);
	}

	
}
