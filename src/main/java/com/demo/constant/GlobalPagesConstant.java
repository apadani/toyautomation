package com.demo.constant;

/**
 * This class contains global constant message text
 * @author Admin
 *
 */
public class GlobalPagesConstant {
	
	
	   public static final String RequestSent = "Request Sent";
	   public static final String RequestSentEsp = "SOLICITUD ENVIADA";
	   public static final String ThankYouConfirmation = "Thank you! A dealer will contact you soon with a quote on your new";
	   public static final String ThankYouConfirmationEsp= "¡Gracias! Un concesionario se pondrá en contacto contigo con un presupuesto para tu nuevo";

}
