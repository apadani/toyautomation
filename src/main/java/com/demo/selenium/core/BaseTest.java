/*
  Class to initialize all application page objects and manage WebDriver browser
  object. Each and every test script class must extend this. This class does
  not use any of the Selenium APIs directly, and adds support to make this
  framework tool independent.

 */
package com.demo.selenium.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.Status;
import com.demo.enums.DriverType;
import com.demo.pages.HomePage;
import com.demo.report.ReportTestManager;
import com.demo.utilities.Utilities;
//import com.relevantcodes.extentreports.ExtentReports;
//import com.relevantcodes.extentreports.ExtentTest;
//import com.relevantcodes.extentreports.LogStatus;

import io.github.bonigarcia.wdm.WebDriverManager;

public abstract class BaseTest {

	private static final Logger logger = LoggerFactory.getLogger(BaseTest.class);
	private static final String BREAK_LINE = "\n";// "</br>";
	//public static ExtentTest test;
	//public static ExtentReports extent;

	protected HomePage homePage;

	private String browserType;
	private WebDriver driver;
	protected String applicationUrl;
	protected boolean mobileTest = false;

	// protected String userEmail;
	// protected String password;
	protected static String gmailPass;

	private static String osName;

	static String resultPath = "screenshots";

	@BeforeSuite
	public void before() throws Exception {
		// Create Result repository for report.
		//extent = new ExtentReports("target/ToyReport.html", true);
		gmailPass = Configuration.readApplicationFile("GmailPass");
		osName = System.getProperty("os.name").toLowerCase();
	}

	@BeforeMethod
	public void setUp() throws Exception {

		browserType = Configuration.readApplicationFile("Browser");
		String chromeProfile = Configuration.readApplicationFile("chromeProfile");
		System.out.println(browserType);

		this.applicationUrl = Configuration.readApplicationFile("URL");

		System.out.println(applicationUrl);
		String systemUser = System.getProperty("user.name");

		if (DriverType.Firefox.toString().toLowerCase().equals(browserType.toLowerCase())) {
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
		} else if (DriverType.IE.toString().toLowerCase().equals(browserType.toLowerCase())) {
			WebDriverManager.iedriver().setup();
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			driver = new InternetExplorerDriver(capabilities);
		} else if (DriverType.Chrome.toString().toLowerCase().equals(browserType.toLowerCase())) {
			
			WebDriverManager.chromedriver().setup();
			ChromeOptions options = new ChromeOptions();
			if(mobileTest) {
				Map<String, String> mobileEmulation = new HashMap<>();
				mobileEmulation.put("deviceName", "iPhone X");
				options.setExperimentalOption("mobileEmulation", mobileEmulation);	
			}
			System.out.print(osName);
			System.out.println(systemUser);

			if (chromeProfile.equals("ON")) {
				
				this.closeChromeBrowse();
				if (osName.contains("mac"))
					options.addArguments("user-data-dir=//Users//" + systemUser
							+ "//Library//Application Support//Google//Chrome//Default");

				if (osName.contains("win"))
					options.addArguments("user-data-dir=C:\\Users\\" + systemUser
							+ "\\AppData\\Local\\Google\\Chrome\\User Data\\Default");
			}

			driver = new ChromeDriver(options);
		} else {
			throw new Exception("Please pass a valid browser type value");
		}

		// Maximize window
		driver.manage().window().maximize();

		// Delete cookies
		driver.manage().deleteAllCookies();

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// Open application URL
		getWebDriver().navigate().to(applicationUrl);
	}

	@AfterMethod
	public void afterMainMethod(ITestResult result) throws IOException, InterruptedException {

		if (result.getStatus() == ITestResult.FAILURE) {
			//test.log(LogStatus.FAIL, result.getThrowable());
			captureScreenshot();
		}
		//extent.endTest(test);
		driver.quit();
	}

	@BeforeMethod
	public void setTest(Method method) {
		//test = extent.startTest(method.getName(), this.getClass().getName());
		//test.assignAuthor("Asif");
		//test.assignCategory(this.getClass().getSimpleName());

	}

	@AfterSuite
	public void tearDownSuite() throws IOException {
		//extent.flush();
		// extent.close();
	}

	public WebDriver getWebDriver() {
		return driver;
	}

	public void closeChromeBrowse() throws IOException {
		Runtime runtime = Runtime.getRuntime();
		if (osName.contains("win")) {
			Runtime.getRuntime().exec("taskkill /F /IM chromedriver.exe");
			runtime.exec("taskkill /F /IM chrome.exe");
		}

		if (osName.contains("mac"))
			runtime.exec("pkill -a -i \"Google Chrome\"");

	}

	/** Capturing screenshot once script is failed */
	public void captureScreenshot() {
		String fileName = System.getProperty("className");
		try {
			String screenshotName = Utilities.getFileName(fileName);
			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			String screen = null;
			String path = resultPath + "/" + fileName;
			new File(path).mkdirs();
			screen = path + "/" + "Failed_" + screenshotName + ".png";
			File screenshotLocation = new File(screen);
			FileUtils.copyFile(screenshot, screenshotLocation);
			Thread.sleep(2000);
			InputStream is = new FileInputStream(screenshotLocation);
			byte[] imageBytes = IOUtils.toByteArray(is);
			Thread.sleep(2000);
			String base64 = Base64.getEncoder().encodeToString(imageBytes);

			//test.log(LogStatus.FAIL, "Failed_" + fileName + " \n Snapshot below: "
					//+ test.addBase64ScreenShot("data:image/png;base64," + base64));
			Reporter.log(
					"<a href= '" + screen + "'target='_blank' ><img src='" + screen + "'>" + screenshotName + "</a>");
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
	}

	/**
	 * Report logs
	 *
	 * @param : message
	 */
	public void reportLog(String message) {
		if(ReportTestManager.getTest() != null)
			ReportTestManager.getTest().log(Status.PASS, message);
		logger.info("Message: " + message);
		message = BREAK_LINE + message;
		Reporter.log(message);
	}

	public static void main(String[] args) {
		System.out.println(System.getProperty("user.name"));
	}
	
	public String getComment(String comment) throws Exception{
		String status = Configuration.readApplicationFile("AllowExistingMail");
		if(status.equalsIgnoreCase("on"))
			return comment;
		else
			return comment + " "+ Utilities.getTimeStamp();
	}

}