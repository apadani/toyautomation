package com.demo.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.demo.datamodel.RequestQuoteModel;
import com.demo.selenium.core.BasePage;
import com.demo.utilities.Utilities;

public class SearchInventoryPage extends BasePage {

	public SearchInventoryPage(WebDriver driver) {
		super(driver);	
	}
	
	
	@FindBy(xpath = "//button[contains(text(), 'View Details')]")
	private WebElement viewDetailsButton;
	
	@FindBy(xpath = "//button[contains(text(), 'Ver detalles')]")
	private WebElement viewDetailsButtonEsp;

	@FindBy(xpath = "(//div[contains(text(), 'Total MSRP (As Built)')]/following-sibling::div)[2]")
	private WebElement priceElement;
	
	@FindBy(xpath = "//tr[td[text()= 'Total MSRP (As Built)']]/td[2]")
	private WebElement priceElementMobile;
	
	@FindBy(xpath = "(//td[text()='Total MSRP (tal cual)']/following-sibling::td)[2]")
	private WebElement priceElementEsp;
	
	@FindBy(xpath = "(//td[text()='Total MSRP (tal cual)']/following-sibling::td)[1]")
	private WebElement priceElementEspMobile;
	
	@FindBy(xpath = "//a[contains(text(), 'View Local Offers')]")
	private WebElement viewLocalOffers;
	
	@FindBy(xpath = "(//a[contains(text(), 'Apply for Financing')])[2]")
	private WebElement applyForFinancing;
	
	@FindBy(xpath = "//a[contains(text(), 'Ver Ofertas Locales')]")
	private WebElement viewLocalOffersEsp;
	
	@FindBy(xpath = "(//a[contains(text(), 'Financiamiento')])[2]")
	private WebElement applyForFinancingEsp;
	
	public SearchInventoryPage selectCarBySeries(RequestQuoteModel data) {
		String year = Utilities.getSeriesYear(data);
		String seriesName = Utilities.getSeriesName(data);
		String locator = "//*[@class='grade']/span[text()='"+year+" ']/parent::div/span[text()='"+seriesName+"']";
		waitForElement(locator);
		WebElement element = driver.findElement(ByLocator(locator));
		javascriptButtonClick(element);
		return PageFactory.initElements(driver, SearchInventoryPage.class);
	}
	
	public SearchInventoryPage selectCarBySeriesEsp(RequestQuoteModel data) {
		String year = Utilities.getSeriesYearEsp(data);
		String seriesName = Utilities.getSeriesNameEsp(data);
		String locator = "//*[@class='grade']/span[text()='"+year+"']/parent::div/span[text()='"+seriesName+" ']";
		waitForElement(locator);
		WebElement element = driver.findElement(ByLocator(locator));
		javascriptButtonClick(element);
		return PageFactory.initElements(driver, SearchInventoryPage.class);
	}
	
	public void clickViewDetails(){
		clickOn(viewDetailsButton);
	}
	
	public void clickViewDetailsEsp(){
		clickOn(viewDetailsButtonEsp);
	}
	
	public RequestQuoteModel getPrice(RequestQuoteModel data){
		waitForElement(priceElement);
		String str = priceElement.getText();
		String priceVehicle= str.replaceAll("[^0-9]", "");
		data.setPrice(priceVehicle);
		return data;
	}
	
	public RequestQuoteModel getPriceMobile(RequestQuoteModel data){
		waitForElement(priceElementMobile);
		String str = priceElementMobile.getText();
		String priceVehicle= str.replaceAll("[^0-9]", "");
		data.setPrice(priceVehicle);
		return data;
	}
	
	public RequestQuoteModel getPriceEsp(RequestQuoteModel data){
		waitForElement(priceElementEsp);
		String str = priceElementEsp.getText();
		String priceVehicle= str.replaceAll("[^0-9]", "");
		data.setPrice(priceVehicle);
		return data;
	}
	
	public RequestQuoteModel getPriceEspMobile(RequestQuoteModel data){
		waitForElement(priceElementEspMobile);
		String str = priceElementEspMobile.getText();
		String priceVehicle= str.replaceAll("[^0-9]", "");
		data.setPrice(priceVehicle);
		return data;
	}
	
	/**
	 * Verify request sent page "Search vendor" and "Build your own" buttons
	 */
	public void verifySearchInventoryButtonsAfterSubmit() {
		Assert.assertTrue(isElementPresent(viewLocalOffers), "View Local Offers button not found");
		Assert.assertTrue(isElementPresent(applyForFinancing), "Apply for Financing button not found");
	}
	
	/**
	 * Verify request sent page "Search vendor" and "Build your own" buttons
	 */
	public void verifySearchInventoryButtonsAfterSubmitEsp() {
		Assert.assertTrue(isElementPresent(viewLocalOffersEsp), "View Local Offers button not found");
		Assert.assertTrue(isElementPresent(applyForFinancingEsp), "Apply for Financing button not found");
	}
	
}
