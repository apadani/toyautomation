package com.demo.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.demo.datamodel.RequestQuoteModel;
import com.demo.selenium.core.BasePage;
import com.demo.utilities.Utilities;

public class BuildAndPricePage extends BasePage {

	public BuildAndPricePage(WebDriver driver) {
		super(driver);
	}

	@FindBy(css = "[class^='tcom-vehicle-lineup-vehicles'] div[class='tcom-vehicle-card-image']:nth-child(1)")
	private WebElement firstCarImage;

	@FindBy(css = "[class='steps-buttons clearfix'] button")
	private WebElement nextButton;

	@FindBy(css = "[id='hero'] [data-flyout-form='REQUEST_QUOTE_EFC']")
	private WebElement requestAQuoteButton;

	@FindBy(css = "[class^='nav model']")
	private WebElement modelTab;
	
	@FindBy(css = "[class^='nav configuration']")
	private WebElement engienTab;
	
	@FindBy(css = "[class^='nav color']")
	private WebElement colorTab;
	
	@FindBy(css = "[class^='nav package']")
	private WebElement packageTab;
	
	@FindBy(css = "[class^='nav accessory']")
	private WebElement accessoryTab;
	
	@FindBy(css = "[class='navs'] [class^='nav summary']")
	private WebElement summaryTab;
	
	@FindBy(css = "[class='configured-vehicle-details-expand']")
	private WebElement showmoreButton;
		
	@FindBy(xpath = "(//*[text()='Total MSRP'])[1]/following-sibling::span")
	private WebElement priceElement;
	
	@FindBy(xpath = "(//*[text()='MSRP total'])[1]/following-sibling::span")
	private WebElement priceElementEsp;
	
	public BuildAndPricePage selectCarBySeries(RequestQuoteModel data) {
		String year = Utilities.getSeriesYear(data);
		String seriesName = Utilities.getSeriesName(data);
		String locator = "//*[@class='grade']/span[text()='"+year+" ']/parent::div/span[text()='"+seriesName+"']";
		waitForElement(locator);
		WebElement element = driver.findElement(ByLocator(locator));
		javascriptButtonClick(element);
		return PageFactory.initElements(driver, BuildAndPricePage.class);
	}

	public BuildAndPricePage selectCarBySeriesEsp(RequestQuoteModel data) {
		String year = Utilities.getSeriesYearEsp(data);
		String seriesName = Utilities.getSeriesNameEsp(data);
		String locator = "//*[@class='grade']/span[text()='"+year+"']/parent::div/span[text()='"+seriesName+" ']";
		waitForElement(locator);
		WebElement element = driver.findElement(ByLocator(locator));
		javascriptButtonClick(element);
		return PageFactory.initElements(driver, BuildAndPricePage.class);
	}
	
	public BuildAndPricePage clickNextButton() throws InterruptedException {
		Thread.sleep(1000);
		waitForElement(nextButton);
		javascriptButtonClick(nextButton);
		return PageFactory.initElements(driver, BuildAndPricePage.class);
	}

	public RequestAQuotePage clickRaqButton() {
		waitAndClick(requestAQuoteButton);
		return PageFactory.initElements(driver, RequestAQuotePage.class);
	}
	
	public RequestQuoteModel getPrice(RequestQuoteModel data){
		waitForElement(priceElement);
		String str = priceElement.getText();
		String priceVehicle= str.replaceAll("[^0-9]", "");
		data.setPrice(priceVehicle);
		return data;
	}
	
	public RequestQuoteModel getPriceEsp(RequestQuoteModel data){
		waitForElement(priceElementEsp);
		String str = priceElementEsp.getText();
		String priceVehicle= str.replaceAll("[^0-9]", "");
		data.setPrice(priceVehicle);
		return data;
	}
	
	public void verifySelectedTab(String tabName){
		String attribute = "Active";
		String attributeName = "class";
		switch(tabName) {
			case "model":
				attribute = super.getAttribute(modelTab, attributeName);
				break;
			case "engien":
				attribute = super.getAttribute(engienTab, attributeName);
				break;	
			case "color":
				attribute = super.getAttribute(colorTab, attributeName);
				break;
				
			case "package":
				attribute = super.getAttribute(packageTab, attributeName);
				break;
		
			case "accessory":
				attribute = super.getAttribute(accessoryTab, attributeName);
				break;
		
			case "summary":
				attribute = super.getAttribute(summaryTab, attributeName);
				break;				
			}
		Assert.assertTrue(attribute.contains("is-active"));
		
	}
}