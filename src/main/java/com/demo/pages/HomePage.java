package com.demo.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.demo.selenium.core.BasePage;

public class HomePage extends BasePage {

	public HomePage(WebDriver driver) {
		super(driver);
	}

	/*
	 * @FindBy(xpath = "//span[text()='Shopping Tools']") private WebElement
	 * shoppingToolMenu;
	 */
	@FindBy(css = "[class='hamburger']")
	private WebElement hamburgerMobile;
	
	@FindBy(css = "[class='shopping-tools primary'] button")
	private WebElement shoppingToolMenuMobile;
	
	@FindBy(css = "[class='guía-de compra primary'] button")
	private WebElement shoppingToolMenuMobileEsp;
	
	@FindBy(css = "[data-service='shoppingTools']")
	private WebElement shoppingToolMenu;
	
	@FindBy(css = "[href='/local-specials/']")
	private WebElement localSpecials;

	@FindBy(css = "[class='ofertas-locales primary'] a")
	private WebElement localSpecialsEspMobile;

	@FindBy(css = "[href='/espanol/local-specials/']")
	private WebElement localSpecialsEsp;

	@FindBy(xpath = "//a[@href='/request-a-quote/']")
	private WebElement requestAQuote;

	@FindBy(xpath = "//div[@class='secondary-links-wrapper']//li[@class='pedir-cotización']/a")
	private WebElement requestAQuoteEsp;
	
	@FindBy(css = "[class='pedir-cotización primary'] a")
	private WebElement requestAQuoteMobileEsp;
	
	@FindBy(xpath = "//div[@id='tcom-shopping-tools-drawer']//span[text()='Contact A Dealer']")
	private WebElement contactADealer;

	@FindBy(css = "[href='/configurator/']")
	private WebElement buildAndPrice;

	@FindBy(css = "[href='/espanol/configurator/']")
	private WebElement buildAndPriceEsp;
	
	@FindBy(css = "[href='/search-inventory/']")
	private WebElement searchInventory;

	@FindBy(css = "[href='/espanol/search-inventory/']")
	private WebElement searchInventoryEsp;	
	
	@FindBy(css = "[id='tcom-header'] [href='/espanol/']")
	private WebElement espanolMenu;	
	
	@FindBy(xpath = "//*[@class='tcom-footer-links-title'][text()='Language']")
	private WebElement lanugageFooterMobileMenu;
	
	@FindBy(css = "[class='language-toggle']")
	private WebElement espanolFooterMobileMenu;

	/**
	 * Select espanol language 
	 */	
	public void selectEspanolLanguage(){
		javascriptButtonClick(espanolMenu);
	}
	
	/**
	 * Select espanol language 
	 */	
	public void selectEspanolLanguageMobile(){
		javascriptButtonClick(lanugageFooterMobileMenu);
		javascriptButtonClick(espanolFooterMobileMenu);
	}
	
	@FindBy(css = "[id='find-dealer']")
	private WebElement findDealers;

	public void selectShoppingToolMenu() {
		javaScriptClick(shoppingToolMenu);
	}

	public RequestAQuotePage gotofindDealers() {
		waitForElement(findDealers);
		javaScriptClick(findDealers);
		return PageFactory.initElements(driver, RequestAQuotePage.class);
	}

	public RequestAQuotePage gotoRequestQuote() {
		waitForElement(requestAQuote);
		javaScriptClick(requestAQuote);
		return PageFactory.initElements(driver, RequestAQuotePage.class);
	}
	
	public RequestAQuotePage gotoRequestQuoteMobile() {
		waitForElement(hamburgerMobile);
		javaScriptClick(hamburgerMobile);
		waitForElement(shoppingToolMenuMobile);
		javaScriptClick(shoppingToolMenuMobile);
		waitForElement(requestAQuote);
		javaScriptClick(requestAQuote);
		return PageFactory.initElements(driver, RequestAQuotePage.class);
	}

	public RequestAQuotePage gotoRequestQuoteMobileEsp() {
		waitForElement(hamburgerMobile);
		javaScriptClick(hamburgerMobile);
		waitForElement(shoppingToolMenuMobileEsp);
		javaScriptClick(shoppingToolMenuMobileEsp);
		waitForElement(requestAQuoteMobileEsp);
		javaScriptClick(requestAQuoteMobileEsp);
		return PageFactory.initElements(driver, RequestAQuotePage.class);
	}
	
	public RequestAQuotePage gotoRequestQuoteESP() {		
		javascriptButtonClick(requestAQuoteEsp);
		return PageFactory.initElements(driver, RequestAQuotePage.class);
	}
	
	public SearchInventoryPage gotoSearchInventory() {
		waitForElement(searchInventory);
		javascriptButtonClick(searchInventory);
		return PageFactory.initElements(driver, SearchInventoryPage.class);
	}

	public SearchInventoryPage gotoSearchInventoryMobile() {
		waitForElement(hamburgerMobile);
		javaScriptClick(hamburgerMobile);
		waitForElement(shoppingToolMenuMobile);
		javaScriptClick(shoppingToolMenuMobile);
		waitForElement(searchInventory);
		javascriptButtonClick(searchInventory);
		return PageFactory.initElements(driver, SearchInventoryPage.class);
	}

	public SearchInventoryPage gotoSearchInventoryEspMobilePage(){
		waitForElement(hamburgerMobile);
		javaScriptClick(hamburgerMobile);
		waitForElement(shoppingToolMenuMobileEsp);
		javaScriptClick(shoppingToolMenuMobileEsp);
		waitForElement(searchInventoryEsp);
		javaScriptClick(searchInventoryEsp);
		return PageFactory.initElements(driver, SearchInventoryPage.class);
	}
	
	
	public SearchInventoryPage gotoSearchInventoryEsp() {
		javascriptButtonClick(searchInventoryEsp);
		return PageFactory.initElements(driver, SearchInventoryPage.class);
	}

	public RequestAQuotePage gotoContactDealer() {
		waitAndClick(contactADealer);
		return PageFactory.initElements(driver, RequestAQuotePage.class);
	}

	public BuildAndPricePage gotoBuildAndPricePage() {
		waitForElement(buildAndPrice);
		javaScriptClick(buildAndPrice);
		return PageFactory.initElements(driver, BuildAndPricePage.class);
	}

	public BuildAndPricePage gotoBuildAndPriceMobilePage() {
		waitForElement(hamburgerMobile);
		javaScriptClick(hamburgerMobile);
		waitForElement(shoppingToolMenuMobile);
		javaScriptClick(shoppingToolMenuMobile);
		waitForElement(buildAndPrice);
		javaScriptClick(buildAndPrice);
		return PageFactory.initElements(driver, BuildAndPricePage.class);
	}

	public BuildAndPricePage gotoBuildAndPriceEspMobilePage(){
		waitForElement(hamburgerMobile);
		javaScriptClick(hamburgerMobile);
		waitForElement(shoppingToolMenuMobileEsp);
		javaScriptClick(shoppingToolMenuMobileEsp);
		waitForElement(buildAndPriceEsp);
		javaScriptClick(buildAndPriceEsp);
		return PageFactory.initElements(driver, BuildAndPricePage.class);
	}
	
	public BuildAndPricePage gotoBuildAndPriceEspPage(){
		waitAndClick(buildAndPriceEsp);
		return PageFactory.initElements(driver, BuildAndPricePage.class);		
	}
	
	public LocalSpecialPage gotoLocalSpecial() {
		waitForElement(localSpecials);
		javaScriptClick(localSpecials);
		return PageFactory.initElements(driver, LocalSpecialPage.class);
	}
	
	public LocalSpecialPage gotoLocalSpecialMobile() {
		waitForElement(hamburgerMobile);
		javaScriptClick(hamburgerMobile);
		waitForElement(shoppingToolMenuMobile);
		javaScriptClick(shoppingToolMenuMobile);
		waitForElement(localSpecials);
		javaScriptClick(localSpecials);
		return PageFactory.initElements(driver, LocalSpecialPage.class);
	}
	
	public LocalSpecialPage gotoLocalSpecialEspMobile() {
		waitForElement(hamburgerMobile);
		javaScriptClick(hamburgerMobile);
		waitForElement(shoppingToolMenuMobileEsp);
		javaScriptClick(shoppingToolMenuMobileEsp);
		waitForElement(localSpecialsEspMobile);
		javaScriptClick(localSpecialsEspMobile);
		return PageFactory.initElements(driver, LocalSpecialPage.class);
	}
	
	
	public LocalSpecialPage gotoLocalSpecialEsp() {
		javascriptButtonClick(localSpecialsEsp);
		return PageFactory.initElements(driver, LocalSpecialPage.class);
	}
}
