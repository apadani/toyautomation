package com.demo.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.demo.selenium.core.BasePage;

public class DisclaimerPage extends BasePage{
	
	public DisclaimerPage(WebDriver driver) {
		super(driver);
	}

	@FindBy(css = "[class='tcom-ad-disclaimer-list-items'] li")
	private List<WebElement> disclaimerListObj;
	
	public List<String> getDisclaimerList() throws Exception{
		List<String> disclaimerData = new ArrayList<String>();
		if(disclaimerListObj.size() == 0)
			throw new Exception("No element found");
		for(WebElement element:disclaimerListObj ) {
			String str = element.getText();
			disclaimerData.add(str);
		}
		return disclaimerData;
	}
	
}
