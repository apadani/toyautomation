package com.demo.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.By;
import com.demo.datamodel.RequestQuoteModel;
import com.demo.selenium.core.BasePage;

public class LocalSpecialPage extends BasePage {

	public LocalSpecialPage(WebDriver driver) {
		super(driver);
	}

	@FindBy(css ="[class='view-details-link'] span")
	private WebElement viewDetail;
	
	@FindBy(xpath="//*[contains(@class, 'slick-current slick-center')]//a[text()='Request A Quote']")
	private WebElement requestAQuoteButton;
	
	@FindBy(xpath="//*[contains(@class, 'slick-current slick-center')]//a[text()='PEDIR COTIZACIÓN']")
	private WebElement requestAQuoteButtonEsp;
	
	@FindBy(css ="[class='tcom-submit active']")
	private WebElement submitZipCodeButton;
	
	@FindBy(css = "[placeholder='Zip Code']")
	private WebElement zipInputBox;

	//@FindBy(css ="[class='standard-offer']")
	//private WebElement seriesNameField;
	
	@FindBy(css ="[class='tcom-incentive-detail'] [class='standard-offer']")
	private WebElement seriesNameField;
	
	private final String str = "//*[contains(@class, 'modal-group-item slick-slide') "
			+ "and not(contains(@aria-hidden, 'true'))]//*[@class='standard-offer']";
	@FindBy(xpath =str)
	private WebElement seriesNameFieldEsp;
	
	public String getSeriesName(){
		waitForElement(seriesNameFieldEsp);
		return getText(seriesNameFieldEsp);
	}

	public String getSeriesNameEsp(){
		waitForElement(seriesNameFieldEsp);
		String seriesYear = str + "//*[@class='series-year']";
		String seriesId = str + "//*[@class='series-id']";
		WebElement elementYear = driver.findElement(By.xpath(seriesYear));
		WebElement elementId = driver.findElement(By.xpath(seriesId));
		return elementYear.getText() + " "+elementId.getText();
	}
	
	/**
	 * Click on View detail button of first product
	 */
	public LocalSpecialPage clickViewDetails(RequestQuoteModel data){
	
		String locator = "//*[@class='offer-ribbon' and text()=' "+data.getOfferRibbon().toLowerCase()+" ']/parent::div/button[@class='view-details-link']";
		waitForElement(locator);
		WebElement element = driver.findElement(ByLocator(locator));
		javascriptButtonClick(element);		
		return this;
	}
	
	
	/**
	 * Click on View detail button of first product
	 */
	public RequestAQuotePage clickRequestAQuoteButton(){
		super.waitAndClick(requestAQuoteButton);
		return PageFactory.initElements(driver, RequestAQuotePage.class);
	}
	
	/**
	 * Click on View detail button of first product
	 */
	public RequestAQuotePage clickRequestAQuoteButtonEsp(){
		super.waitAndClick(requestAQuoteButtonEsp);
		return PageFactory.initElements(driver, RequestAQuotePage.class);
	}
}