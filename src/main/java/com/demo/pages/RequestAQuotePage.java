package com.demo.pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.demo.datamodel.RequestQuoteModel;
import com.demo.selenium.core.BasePage;

public class RequestAQuotePage extends BasePage {

	public RequestAQuotePage(WebDriver driver) {
		super(driver);
	}

	final String prefix = "modal-group-item slick-slide slick-current slick-center";

	@FindBy(css = "[name='seriesName']")
	private WebElement seriesName;

	@FindBy(css = "[name='modelName']")
	private WebElement modelName;

	@FindBy(css = "[class='" + prefix + "'] [name='fullName']")
	private WebElement fullNameInputLocalSpecial;

	@FindBy(css = "[class='" + prefix + "'] [name=\"firstName\"]")
	private WebElement firstNameLocalSpecial;

	@FindBy(css = "[class='" + prefix + "'] [name='lastName']")
	private WebElement lastNameInputLocalSpecial;

	@FindBy(css = "[class='" + prefix + "'] [name='emailAddress']")
	private WebElement emailAddressInputLocalSpecial;

	@FindBy(css = "[class='" + prefix + "'] [name='phone']")
	private WebElement phoneInputLocalSpecial;

	@FindBy(css = "[class='" + prefix + "'] [name='address1']")
	private WebElement address1InputLocalSpecial;

	@FindBy(css = "[class='" + prefix + "'] [name='city']")
	private WebElement cityInputLocalSpecial;

	@FindBy(css = "[class='" + prefix + "'] [class='raq-add-comments-btn']")
	private WebElement addCommentButtonLocalSpecial;

	@FindBy(css = "[class='" + prefix + "'] [name='comments']")
	private WebElement commentTextAreaLocalSpecial;

	@FindBy(css = "[class='" + prefix + "'] [value='SUBMIT']")
	private WebElement submitButtonLocalSpecial;

	@FindBy(css = "[class='" + prefix + "'] [class='tcom-dealer-select-list'] label span")
	private WebElement firstVendorNameLocalSpecial;

	@FindBy(css = "[class='" + prefix + "'] [class='tcom-dealer-select-list'] label input")
	private WebElement firstVendorNameCheckBoxLocalSpecial;
	
	@FindBy(css = "[class='" + prefix + "'] [name='dealerCode']")
	private WebElement dealerCodeLocalSpecial;

	@FindBy(css = "[name='fullName']")
	private WebElement fullNameInput;

	@FindBy(css = "[name='emailAddress']")
	private WebElement emailAddressInput;

	@FindBy(css = "[name='phone']")
	private WebElement phoneInput;

	@FindBy(css = "[name='address1']")
	private WebElement address1Input;

	@FindBy(css = "[name='city']")
	private WebElement cityInput;

	@FindBy(css = "[name='firstName']")
	private WebElement firstName;

	@FindBy(css = "[name='lastName']")
	private WebElement lastNameInput;

	@FindBy(css = "[class='raq-dealers-container'] [placeholder='Zip Code']")
	private WebElement zipCodeInput;

	@FindBy(css = "[class='raq-submit-buttons'] button:nth-child(1)")
	private WebElement submitButton;

	@FindBy(css = "[class='form-header']")
	private WebElement messageField;

	@FindBy(css = "[class='raq-confirmation-message']")
	private WebElement raqConfirmationMessage;

	@FindBy(css = "[class='raq-confirmation-grade']")
	private WebElement raqConfirmationGradeMessage;

	@FindBy(name = "zipcode")
	private WebElement zipInputBox;

	// @FindBy(css = "[data-di-id='#tcom-submit-active']")
	@FindBy(css = "[class='zipcode-form-wrapper'] [name='button']")
	private WebElement zipSubmitButton;

	@FindBy(css = "[class='tcom-dealer-select-list'] label span")
	private WebElement firstVendorName;
	
	@FindBy(xpath = "(//*[@class='tcom-dealer-select-list']//label/input)[1]")
	private WebElement firstVendorCheckBox;

	@FindBy(css = "[name='dealerCode']")
	private WebElement dealerCode;

	@FindBy(xpath = "//a[@rel='external'][text()='Search Inventory ']")
	private WebElement searchInventoryButton;

	@FindBy(xpath = "//a[@rel='external'][text()='Busca en el inventario ']")
	private WebElement searchInventoryButtonEsp;
	
	@FindBy(xpath = "//a[@rel='external'][text()='Build Your Own']")
	private WebElement buildYourOwnButton;

	@FindBy(xpath = "//a[@rel='external'][text()='DISEÑA Y COTIZA']")
	private WebElement buildYourOwnButtonEsp;

	@FindBy(xpath = "//a[@rel='external'][text()='Continúa con tu diseño']")
	private WebElement continueYourBuildButtonEsp;

	@FindBy(css = "[data-di-id='#pma-toggle']")
	private WebElement viewMore;

	@FindBy(css = "[class='raq-add-comments-btn']")
	private WebElement addCommentButton;

	@FindBy(css = "[name='comments']")
	private WebElement commentTextArea;

	// @FindBy(css = "[class='zipcode has-icon-pencil zipcode-cta']")
	// @FindBy(css = "[class=\"tcom-icon tcom-icon-pencil\"]")
	@FindBy(css = "[class=\"zipcode has-icon-pencil zipcode-cta\"]")
	private WebElement topZipcodePencilIcon;
	
	@FindBy(css = "[data-di-id='#zipcode-btn']")
	private WebElement topZipcodeIconMobile;

	@FindBy(xpath = "//a[@rel='external'][text()='Continue Your Build']")
	private WebElement continueYourBuildButton;

	@FindBy(css = "[data-di-id='#name']")
	private WebElement DealerName;

	@FindBy(css = "[data-di-id='#tfresh-btn-tfresh-btn-one']")
	private WebElement RequestAQuoteQDealersButton;

	/**
	 * Submit enter zipcode
	 * 
	 * @param model
	 */
	public void submitZipCode(RequestQuoteModel model) throws InterruptedException {
		Thread.sleep(1000);
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		boolean status = isElementDisplay(zipInputBox);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		if (status) {
			inputTextWitClear(zipInputBox, model.getZipCode());
			System.out.println(model.getZipCode());
			javascriptButtonClick(zipSubmitButton);
		} else {
			waitAndClick(topZipcodePencilIcon);
			inputTextWitClear(zipInputBox, model.getZipCode());
			System.out.println("Pencil: " + model.getZipCode());
			Thread.sleep(1000);
			clickOn(zipSubmitButton);
			Thread.sleep(2000);
		}
	}

	/**
	 * Submit enter zipcode
	 * 
	 * @param model
	 */
	public void submitZipCodeMobile(RequestQuoteModel model) throws InterruptedException {
		Thread.sleep(1000);
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		boolean status = isElementDisplay(zipInputBox);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		if (status) {
			inputTextWitClear(zipInputBox, model.getZipCode());
			System.out.println(model.getZipCode());
			javascriptButtonClick(zipSubmitButton);
		} else {
			javaScriptClick(topZipcodeIconMobile);
			inputTextWitClear(zipInputBox, model.getZipCode());
			System.out.println("Pencil: " + model.getZipCode());
			Thread.sleep(1000);
			clickOn(zipSubmitButton);
			Thread.sleep(2000);
		}
	}
	
	public String getMessage() {
		return getText(messageField);
	}

	public String getConfirmationThankYouMessage() {
		return getText(raqConfirmationMessage);
	}

	public String getConfirmationGradeMessage() {
		return getText(raqConfirmationGradeMessage);
	}

	/**
	 * Fill Request quote form
	 * 
	 * @param model
	 * @throws InterruptedException
	 */
	public void fillRequestQuoteDetail(RequestQuoteModel model) throws InterruptedException {
		Thread.sleep(2000);
		selectDropDownByText(seriesName, model.getSeriesName());
		Thread.sleep(5000);
		selectDropDownByText(modelName, model.getModelName());
		Thread.sleep(2000);
		clickOn(fullNameInput);
		inputText(firstName, model.getFirstName());
		inputText(lastNameInput, model.getLastName());
		inputText(emailAddressInput, model.getEmail());
		inputText(phoneInput, model.getPhone());
		inputText(address1Input, model.getAddress());
		inputText(cityInput, model.getCity());
		javaScriptClick(addCommentButton);
		inputText(commentTextArea, model.getCommentText());
		System.out.println(model.isSet());
		if(model.isSet()) {
			boolean status = firstVendorCheckBox.isSelected();
			clickOn(firstVendorName);
			Assert.assertEquals(status, false, "Vendor check box is selected for SET");
		}
		boolean status = firstVendorCheckBox.isSelected();
		Assert.assertEquals(status, true, "Vendor check box is selected");
		clickOn(emailAddressInput);

	}

	/**
	 * Click on submit button
	 */
	public void clickSubmitButton() {
		waitAndClick(submitButton);
	}

	/**
	 * Click on submit button
	 */
	public void clickRAQLocalSpecialSubmit() {
		waitAndClick(submitButtonLocalSpecial);
	}

	/*
	 
	  */
	public void clickRequestAQuoteDealerButton() {
		waitForElement(RequestAQuoteQDealersButton);
		javascriptButtonClick(RequestAQuoteQDealersButton);
		// waitAndClick(RequestAQuoteQDealersButton);
	}

	/**
	 * Click on view more link
	 */
	public void clickViewMoreLink() {
		waitAndClick(viewMore);
	}

	/**
	 * Get first vendor name from vendor list
	 * 
	 * @return
	 */
	public String getVendorName() {
		waitForElement(firstVendorName);
		return firstVendorName.getText();
	}

	/**
	 * Get first dealer code from vendor list
	 * 
	 * @return
	 */
	public String dealerCode() {
		return dealerCode.getAttribute("value");
	}

	public String getDealerName() {
		return DealerName.getText();
	}

	/**
	 * Get first vendor name from vendor list
	 * 
	 * @return
	 */
	public String getVendorNameLocalSpecial() {
		waitForElement(firstVendorNameLocalSpecial);
		return firstVendorNameLocalSpecial.getText();
	}

	/**
	 * Get first dealer code from vendor list
	 * 
	 * @return
	 */
	public String dealerCodeLocalSpecial() {
		return dealerCodeLocalSpecial.getAttribute("value");
	}

	/**
	 * Verify request sent page "Search vendor" and "Build your own" buttons
	 */
	public void verifyButtons() {
		Assert.assertTrue(isElementPresent(searchInventoryButton), "Search vender button not found");
		Assert.assertTrue(isElementPresent(buildYourOwnButton), "Build your own button not found");
	}

	/**
	 * Verify request sent page "Search vendor" and "Build your own" buttons
	 */
	public void verifyButtonsEsp() {
		Assert.assertTrue(isElementPresent(searchInventoryButtonEsp), "Search vender button not found");
		Assert.assertTrue(isElementPresent(buildYourOwnButtonEsp), "Build your own button not found");
	}
	
	/**
	 * Verify request sent page "Search vendor" and "Build your own" buttons
	 */
	public void verifyBAndPButtonsAfterSubmit() {
		Assert.assertTrue(isElementPresent(searchInventoryButton), "Search vender button not found");
		Assert.assertTrue(isElementPresent(continueYourBuildButton), "Continue your build button not found");
	}
	
	
	public void verifyBAndPButtonsAfterSubmitEsp() {
		Assert.assertTrue(isElementPresent(searchInventoryButtonEsp), "Search vender button not found");
		Assert.assertTrue(isElementPresent(continueYourBuildButtonEsp), "Continue your build button not found");
	}

	
	
	public void fillRQDetailForLocalSpecial(RequestQuoteModel model) {
		waitAndClick(fullNameInputLocalSpecial);
		inputText(firstNameLocalSpecial, model.getFirstName());
		inputText(lastNameInputLocalSpecial, model.getLastName());
		inputText(emailAddressInputLocalSpecial, model.getEmail());
		inputText(phoneInputLocalSpecial, model.getPhone());
		inputText(address1InputLocalSpecial, model.getAddress());
		inputText(cityInputLocalSpecial, model.getCity());		
		clickOn(addCommentButtonLocalSpecial);
		inputText(commentTextAreaLocalSpecial, model.getCommentText());	
		if(model.isSet()) {
			boolean status = firstVendorNameCheckBoxLocalSpecial.isSelected();
			clickOn(firstVendorNameLocalSpecial);
			Assert.assertEquals(status, false, "Vendor check box is selected for SET");
		}
		boolean status = firstVendorNameCheckBoxLocalSpecial.isSelected();
		System.out.println(status);
		Assert.assertEquals(status, true, "Vendor check box is not selected");	
		clickOn(emailAddressInputLocalSpecial);
	}

	/**
	 * Fill Request quote form for local special
	 * 
	 * @param model
	 * @throws InterruptedException
	 */
	public void fillRQDetail(RequestQuoteModel model) throws InterruptedException {
		Thread.sleep(2000);
		waitAndClick(fullNameInput);
		inputText(firstName, model.getFirstName());
		inputText(lastNameInput, model.getLastName());
		// inputText(zipCodeInput, model.getZipCode());
		inputText(emailAddressInput, model.getEmail());
		inputText(phoneInput, model.getPhone());
		inputText(address1Input, model.getAddress());
		inputText(cityInput, model.getCity());
		clickOn(addCommentButton);
		inputText(commentTextArea, model.getCommentText());
		System.out.println(model.isSet());
		if(model.isSet()) {
			boolean status = firstVendorCheckBox.isSelected();
			clickOn(firstVendorName);
			Assert.assertEquals(status, false, "Vendor check box is selected for SET");
		}
		boolean status = firstVendorCheckBox.isSelected();
		Assert.assertEquals(status, true, "Vendor check box is not selected");
		clickOn(emailAddressInput);
	}	
	
}