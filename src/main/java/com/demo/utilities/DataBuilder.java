package com.demo.utilities;

import java.util.ArrayList;
import java.util.List;

import com.demo.datamodel.RequestQuoteModel;

public class DataBuilder {

	public static String  testDataSheet = "TestData.xlsx";
	
	public List<RequestQuoteModel> prepareRequestQuoteData() {
		List<RequestQuoteModel> raqData = new ArrayList<RequestQuoteModel>();
		Read_XLS read = new Read_XLS(testDataSheet, "data/");
		Object[][] objs = new Object[read.retrieveNoOfRows("RAQStandaloneUs") - 1][read.retrieveNoOfCols("RAQStandaloneUs")];

		
		objs = read.retrieveTestData1("RAQStandaloneUs");
		for (Object[] obj : objs) {
			RequestQuoteModel requestQuoteData = new RequestQuoteModel();
			requestQuoteData.setSeriesName(obj[0].toString());
			requestQuoteData.setModelName(obj[1].toString());
			requestQuoteData.setFirstName(obj[2].toString());
			requestQuoteData.setLastName(obj[3].toString());
			requestQuoteData.setEmail(obj[4].toString());			
			requestQuoteData.setPhone(obj[5].toString());
			requestQuoteData.setAddress(obj[6].toString());
			requestQuoteData.setCity(obj[7].toString());
			requestQuoteData.setZipCode(obj[8].toString());
			requestQuoteData.setSet(Boolean.parseBoolean(obj[9].toString()));			
			raqData.add(requestQuoteData);
		}
		return raqData;
	}

	public List<RequestQuoteModel> prepareBuildAndPriceData() {
		List<RequestQuoteModel> raqData = new ArrayList<RequestQuoteModel>();
		Read_XLS read = new Read_XLS(testDataSheet, "data/");
		Object[][] objs = new Object[read.retrieveNoOfRows("RAQBuildAndPriceUs") - 1][read.retrieveNoOfCols("RAQBuildAndPriceUs")];

		objs = read.retrieveTestData1("RAQBuildAndPriceUs");
		for (Object[] obj : objs) {
			RequestQuoteModel requestQuoteData = new RequestQuoteModel();	
			requestQuoteData.setSeriesName(obj[0].toString());
			requestQuoteData.setFirstName(obj[1].toString());
			requestQuoteData.setLastName(obj[2].toString());
			requestQuoteData.setEmail(obj[3].toString());			
			requestQuoteData.setPhone(obj[4].toString());
			requestQuoteData.setAddress(obj[5].toString());
			requestQuoteData.setCity(obj[6].toString());
			requestQuoteData.setZipCode(obj[7].toString());	
			requestQuoteData.setSet(Boolean.parseBoolean(obj[8].toString()));
			raqData.add(requestQuoteData);
		}
		return raqData;
	}

	public List<RequestQuoteModel> prepareLocalSpecialData() {
		List<RequestQuoteModel> raqData = new ArrayList<RequestQuoteModel>();
		Read_XLS read = new Read_XLS(testDataSheet, "data/");
		Object[][] objs = new Object[read.retrieveNoOfRows("RAQLocalSpecialUs") - 1][read.retrieveNoOfCols("RAQLocalSpecialUs")];

		
		objs = read.retrieveTestData1("RAQLocalSpecialUs");

		for (Object[] obj : objs) {
			RequestQuoteModel requestQuoteData = new RequestQuoteModel();	
			requestQuoteData.setOfferRibbon(obj[0].toString());
			requestQuoteData.setFirstName(obj[1].toString());
			requestQuoteData.setLastName(obj[2].toString());
			requestQuoteData.setEmail(obj[3].toString());			
			requestQuoteData.setPhone(obj[4].toString());
			requestQuoteData.setAddress(obj[5].toString());
			requestQuoteData.setCity(obj[6].toString());
			requestQuoteData.setZipCode(obj[7].toString());	
			requestQuoteData.setSet(Boolean.parseBoolean(obj[8].toString()));
			raqData.add(requestQuoteData);
		}
		return raqData;
	}

	public List<RequestQuoteModel> prepareSearchInventoryData() {
		List<RequestQuoteModel> raqData = new ArrayList<RequestQuoteModel>();
		Read_XLS read = new Read_XLS(testDataSheet, "data/");
		Object[][] objs = new Object[read.retrieveNoOfRows("RAQSearchInventoryUs") - 1][read.retrieveNoOfCols("RAQSearchInventoryUs")];

		objs = read.retrieveTestData1("RAQSearchInventoryUs");

		for (Object[] obj : objs) {
			RequestQuoteModel requestQuoteData = new RequestQuoteModel();	
			requestQuoteData.setSeriesName(obj[0].toString());
			requestQuoteData.setFirstName(obj[1].toString());
			requestQuoteData.setLastName(obj[2].toString());
			requestQuoteData.setEmail(obj[3].toString());			
			requestQuoteData.setPhone(obj[4].toString());
			requestQuoteData.setAddress(obj[5].toString());
			requestQuoteData.setCity(obj[6].toString());
			requestQuoteData.setZipCode(obj[7].toString());	
			requestQuoteData.setSet(Boolean.parseBoolean(obj[8].toString()));
			raqData.add(requestQuoteData);
		}
		return raqData;
	}	

}
